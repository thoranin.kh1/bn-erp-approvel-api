package domain

import "github.com/gin-gonic/gin"

type ApprovalHandlerInterface interface {
	GetApprove(c *gin.Context)
	GetApproveByID(c *gin.Context)
	CreateApprove(c *gin.Context)
	UpdateApprove(c *gin.Context)
	DeleteApprove(c *gin.Context)
}

type DocumentHandlerInterface interface {
	GetDocument(c *gin.Context)
	ApproveDocument(c *gin.Context)
	RejectDocument(c *gin.Context)
	GetInformation(c *gin.Context)
	InformationDocument(c *gin.Context)
	ReptyInformationDocument(c *gin.Context)
}

type FormulaHandlerInterface interface {
	GetCorp(c *gin.Context)
	GetBranch(c *gin.Context)
	GetBook(c *gin.Context)
}

type PositionHandlerInterface interface {
	GetPosition(c *gin.Context)
	GetPositionByID(c *gin.Context)
	CreatePosition(c *gin.Context)
	UpdatePosition(c *gin.Context)
	DeletePosition(c *gin.Context)
}

type SettingHandlerInterface interface {
	GetDocumentType(c *gin.Context)
	GetApprovalSetting(c *gin.Context)
	GetApprovalSettingByID(c *gin.Context)
	CreateApprovalSetting(c *gin.Context)
	UpdateApprovalSetting(c *gin.Context)
	DeleteApprovalSetting(c *gin.Context)
}

type UserHandlerInterface interface {
	SignIn(c *gin.Context)
	SignOut(c *gin.Context)
	GetUser(c *gin.Context)
	GetUserByID(c *gin.Context)
	CreateUser(c *gin.Context)
	UpdateUser(c *gin.Context)
	UpdatePasswordUser(c *gin.Context)
	DeleteUser(c *gin.Context)
	GetMenu(c *gin.Context)
	GetUserMenu(c *gin.Context)
}
