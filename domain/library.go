package domain

import (
	"bn-erp-approvel-api/model"
	"time"
)

type EnvironmentLibraryInterface interface {
	SetTimezone(name string) error
	Now() time.Time
}

type ResponseLibraryInterface interface {
	GetResponseSuccess() (int, model.ResponseModel)
	GetResponseSuccessWithData(data interface{}) (int, model.ResponseModel)
	GetResponseCustom(code int, status bool, message string) (int, model.ResponseModel)
	GetResponseCustomWithData(code int, status bool, message string, data interface{}) (int, model.ResponseModel)
	GetResponseBadRequest() (int, model.ResponseModel)
	GetResponseUnauthorized() (int, model.ResponseModel)
	GetResponseInternalServerError() (int, model.ResponseModel)
}

type TokenLibraryInterface interface {
	CreateToken(claims model.TokenClaimsModel) (string, error)
	ValidateToken(encyptToken string) (model.TokenClaimsModel, error)
}
