package domain

import "github.com/gin-gonic/gin"

type MiddlewareInterface interface {
	SetJsonResponse() gin.HandlerFunc
	CORSMiddleware() gin.HandlerFunc
	JwtAuthMiddleware() gin.HandlerFunc
}
