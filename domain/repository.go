package domain

import "bn-erp-approvel-api/model"

type ApproveRepositoryInterface interface {
	Get(filter model.ApprovalFilter) ([]model.Approval, error)
	Create(data model.Approval) (model.Approval, error)
	Update(data model.Approval) (model.Approval, error)
	Delete(id int) error
}

type DocumentRepositoryInterface interface {
	GetApproveData(key string) (model.DocumentApproveList, error)
	GetTransectionProcess(id int) (model.ProcessCommand, error)
	UpdateTransection(transection model.ProcessCommand) error
	GetPRFormulaDocumentData(id string) (model.PRDocument, error)
	GetPBFormulaDocumentData(id string) (model.PBDocument, error)
	GetDocument(filter model.DocumentFilter) ([]model.DocumentApprove, error)
	CreateDocument(document model.DocumentApprove, approves []model.DocumentApproveList) error
	UpdateDocument(document model.DocumentApprove) error
	GetInformation(filter model.FilterInformation) ([]model.DocumentInformation, error)
	UpdateDocumentInformation(document model.DocumentApprove, information model.DocumentInformation) error
	DeleteDocumentByRefID(refID string) error
}

type FormulaRepositoryInterface interface {
	GetCompany(filter model.CompanyFilterModel) ([]model.CompanyModel, error)
	GetBranch(filter model.BranchFilterModel) ([]model.BranchModel, error)
	Getbook(filter model.BookFilterModel) ([]model.BookModel, error)
}

type PositionRepositoryInterface interface {
	Get(filter model.PositionFilter) ([]model.Position, error)
	Create(position model.Position, approval []int) (model.Position, error)
	Update(position model.Position, approval []int) (model.Position, error)
	Delete(id int) error
}

type SettingRepositoryInterface interface {
	GetDocumentType() ([]model.DocumentType, error)
	GetSetting(filter model.SettingFilter) ([]model.Setting, error)
	CreateSetting(setting model.Setting) (model.Setting, error)
	UpdateSetting(setting model.Setting) (model.Setting, error)
	DeleteSetting(id uint) error
}

type UserRepositoryInterface interface {
	GetUser(filter model.UserFilter) ([]model.User, error)
	CreateUser(user model.User, permissions []model.Permission) (model.User, error)
	UpdateUser(user model.User, permissions []model.Permission) (model.User, error)
	UpdateUserProfile(user model.User) error
	DeleteUser(id uint) error
	GetToken(key string) (model.Token, error)
	CreateToken(token model.Token) (model.Token, error)
	DeleteToken(key string) error
	GetMenu() ([]model.Menu, error)
	CreateOrUpdateMenu(menus []string) error
	GetUserPermission(userID int) ([]model.Permission, error)
}
