package domain

import "bn-erp-approvel-api/model"

type ApproveUsecaseInterface interface {
	GetApproval(filter model.ApprovalFilter) ([]model.Approval, error)
	GetApprovalByID(id int) (model.Approval, error)
	Create(req model.ApprovalAction) (model.Approval, error)
	Update(req model.ApprovalAction) (model.Approval, error)
	Delete(id int) error
}

type DocumentUsecaseInterface interface {
	GetDocument(filter model.DocumentFilter) ([]model.DocumentApprove, error)
	Create(id int) error
	ApproveDocument(key string) error
	RejectDocument(key string) error
	GetInformation(filter model.FilterInformation) ([]model.DocumentInformation, error)
	InformationDocument(req model.RequestDocumentInformation) error
	ReptyInformationDocument(req model.ReptyDocumentInformation) error
	ResetDocument(id int) error
}

type PositionUsecaseInterface interface {
	GetPositon() ([]model.Position, error)
	GetPositonByID(id int) (model.Position, error)
	CreatePosition(req model.PositionAction) (model.Position, error)
	UpdatePosition(req model.PositionAction) (model.Position, error)
	DeletePosition(id int) error
}

type FormulaUsecaseInterface interface {
	GetCompany(filter model.CompanyFilterModel) ([]model.CompanyModel, error)
	GetBranch(filter model.BranchFilterModel) ([]model.BranchModel, error)
	Getbook(filter model.BookFilterModel) ([]model.BookModel, error)
	GetbookByID(id string) (model.BookModel, error)
}

type SettingUsecaseInterface interface {
	GetDocumentType() ([]model.DocumentType, error)
	GetSetting(filter model.SettingFilter) ([]model.Setting, error)
	GetSettingByID(id uint) (model.Setting, error)
	CreateSetting(request model.SettingRequest) (model.Setting, error)
	UpdateSetting(request model.SettingRequest) (model.Setting, error)
	DeleteSetting(id uint) error
}

type UserUsecaseInterface interface {
	GetUser() ([]model.User, error)
	GetUserByID(id uint) (model.User, error)
	CreateUser(req model.UserCreate) (model.User, error)
	UpdateUser(req model.UserUpdate) (model.User, error)
	UpdateUserPasword(req model.UserPasswordUpdate) error
	DeleteUser(id uint) error
	SignIn(req model.UserSignin) (model.Token, error)
	HasToken(key string) (bool, error)
	DeleteToken(key string) error
	InitMenu() error
	GetMenu() ([]model.Menu, error)
	GetUserPermission(userID int) ([]model.Permission, error)
}
