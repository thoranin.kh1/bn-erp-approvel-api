package command_action_enums

type CommandActionEnums int32

const (
	INSERT CommandActionEnums = iota
	UPDATE
	DELETE
	INFORMATION
)

var TokenLists = map[CommandActionEnums]string{
	INSERT:      "INSERT",
	UPDATE:      "UPDATE",
	DELETE:      "DELETE",
	INFORMATION: "INFORMATION",
}

func (e CommandActionEnums) String() string {
	if str, ok := TokenLists[e]; ok {
		return str
	}
	return ""
}
