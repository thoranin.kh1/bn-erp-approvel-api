package command_enums

type CommandEnums int32

const (
	EMAIL CommandEnums = iota
	SETTING
	REPROCESS
)

var TokenLists = map[CommandEnums]string{
	EMAIL:     "EMAIL",
	SETTING:   "SETTING",
	REPROCESS: "REPROCESS",
}

func (e CommandEnums) String() string {
	if str, ok := TokenLists[e]; ok {
		return str
	}
	return ""
}
