package document_status_enums

type DocumentStatusEnums int32

const (
	APPROVE DocumentStatusEnums = iota
	REJECT
	PENDING
	INFOMATION
)

var DocumentStatusLists = map[DocumentStatusEnums]string{
	APPROVE:    "APPROVE",
	REJECT:     "REJECT",
	PENDING:    "PENDING",
	INFOMATION: "INFOMATION",
}

func (e DocumentStatusEnums) String() string {
	if str, ok := DocumentStatusLists[e]; ok {
		return str
	}
	return ""
}
