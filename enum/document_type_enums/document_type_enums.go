package document_type_enums

type DocumentTypeEnums int32

const (
	PO DocumentTypeEnums = iota
	PR
	PB
)

var TokenLists = map[DocumentTypeEnums]string{
	PO: "PO",
	PR: "PR",
	PB: "PB",
}

func (e DocumentTypeEnums) String() string {
	if str, ok := TokenLists[e]; ok {
		return str
	}
	return ""
}
