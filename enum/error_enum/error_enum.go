package error_enum

import "errors"

type ErrorTypes int32

const (
	MSSQL ErrorTypes = iota
	FOMULA_MSSQL
	EXISTS
	EXPTY
	USED
	PASSWORD
	TOKEN
	PERMISSION
	STATUS
)

var ErrorLists = map[ErrorTypes]error{
	MSSQL:        errors.New("MSSQL"),
	FOMULA_MSSQL: errors.New("FOMULA_MSSQL"),
	EXISTS:       errors.New("EXISTS"),
	EXPTY:        errors.New("EXPTY"),
	USED:         errors.New("USED"),
	PASSWORD:     errors.New("PASSWORD"),
	TOKEN:        errors.New("TOKEN"),
	PERMISSION:   errors.New("PERMISSION"),
	STATUS:       errors.New("STATUS"),
}

func (e ErrorTypes) Error() error {
	if err, ok := ErrorLists[e]; ok {
		return err
	}
	return nil
}
