package token_enums

type TokenEnums int32

const (
	USER_ID TokenEnums = iota
	TOKEN
)

var TokenLists = map[TokenEnums]string{
	USER_ID: "USER_ID",
	TOKEN:   "TOKEN",
}

func (e TokenEnums) String() string {
	if str, ok := TokenLists[e]; ok {
		return str
	}
	return ""
}
