package handler

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
	"strconv"

	"github.com/gin-gonic/gin"
)

type approveHandler struct {
	ApprovalUsecase domain.ApproveUsecaseInterface
	Responses       domain.ResponseLibraryInterface
}

func NewApproveHandler(
	approvalUsecase domain.ApproveUsecaseInterface,
	response domain.ResponseLibraryInterface,
) domain.ApprovalHandlerInterface {
	return &approveHandler{
		ApprovalUsecase: approvalUsecase,
		Responses:       response,
	}
}

func (h *approveHandler) GetApprove(c *gin.Context) {
	positionID, _ := strconv.Atoi(c.Query("position_id"))
	approvals, err := h.ApprovalUsecase.GetApproval(model.ApprovalFilter{
		PositionID: uint(positionID),
	})
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approvals))
}

func (h *approveHandler) GetApproveByID(c *gin.Context) {
	id := c.Param("id")
	userID, _ := strconv.Atoi(id)
	approval, err := h.ApprovalUsecase.GetApprovalByID(userID)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approval))
}

func (h *approveHandler) CreateApprove(c *gin.Context) {
	request := model.ApprovalAction{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	approval, err := h.ApprovalUsecase.Create(request)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approval))
}

func (h *approveHandler) UpdateApprove(c *gin.Context) {
	id := c.Param("id")
	userID, _ := strconv.Atoi(id)
	request := model.ApprovalAction{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	request.ID = userID

	approval, err := h.ApprovalUsecase.Update(request)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approval))
}

func (h *approveHandler) DeleteApprove(c *gin.Context) {
	id := c.Param("id")
	userID, _ := strconv.Atoi(id)
	err := h.ApprovalUsecase.Delete(userID)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccess())
}
