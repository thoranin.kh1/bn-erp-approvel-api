package handler

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/enum/token_enums"
	"bn-erp-approvel-api/model"
	"bn-erp-approvel-api/util"
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
)

type documentHandler struct {
	DocumentUsecase domain.DocumentUsecaseInterface
	Responses       domain.ResponseLibraryInterface
}

func NewDocumentHandler(
	documentUsecase domain.DocumentUsecaseInterface,
	responses domain.ResponseLibraryInterface,
) domain.DocumentHandlerInterface {
	return &documentHandler{
		DocumentUsecase: documentUsecase,
		Responses:       responses,
	}
}

func (h *documentHandler) GetDocument(c *gin.Context) {
	documents, err := h.DocumentUsecase.GetDocument(model.DocumentFilter{
		BookID:         c.Query("book_id"),
		TypeCode:       c.Query("type_code"),
		DocumentStatus: c.Query("status"),
		DateStart:      c.Query("date_start"),
		DateEnd:        c.Query("date_end"),
		CorpID:         util.QueryDecode(c.Query("corp_id")),
		BranchID:       util.QueryDecode(c.Query("branch_id")),
	})
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(documents))
}

func (h *documentHandler) ApproveDocument(c *gin.Context) {
	transectionKey := c.Query("key")
	err := h.DocumentUsecase.ApproveDocument(transectionKey)
	if err != nil {
		if errors.Is(err, error_enum.STATUS.Error()) {
			c.JSON(h.Responses.GetResponseCustom(420, false, "document status."))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccess())
}

func (h *documentHandler) RejectDocument(c *gin.Context) {
	transectionKey := c.Query("key")
	err := h.DocumentUsecase.RejectDocument(transectionKey)
	if err != nil {
		if errors.Is(err, error_enum.STATUS.Error()) {
			c.JSON(h.Responses.GetResponseCustom(420, false, "document status."))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccess())
}

func (h *documentHandler) GetInformation(c *gin.Context) {
	documentID := c.Query("document_id")
	documentIDInt, _ := strconv.Atoi(documentID)

	filter := model.FilterInformation{
		DocumentID: uint(documentIDInt),
	}
	informations, err := h.DocumentUsecase.GetInformation(filter)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(informations))
}

func (h *documentHandler) InformationDocument(c *gin.Context) {
	request := model.RequestDocumentInformation{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	err := h.DocumentUsecase.InformationDocument(request)
	if err != nil {
		if errors.Is(err, error_enum.STATUS.Error()) {
			c.JSON(h.Responses.GetResponseCustom(420, false, "document status."))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccess())
}

func (h *documentHandler) ReptyInformationDocument(c *gin.Context) {
	request := model.ReptyDocumentInformation{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}
	id := c.GetString(token_enums.USER_ID.String())
	userID, _ := strconv.Atoi(id)
	request.UserID = userID

	err := h.DocumentUsecase.ReptyInformationDocument(request)
	if err != nil {
		if errors.Is(err, error_enum.STATUS.Error()) {
			c.JSON(h.Responses.GetResponseCustom(420, false, "document status."))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccess())
}
