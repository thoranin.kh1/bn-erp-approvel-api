package handler

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
	"bn-erp-approvel-api/util"

	"github.com/gin-gonic/gin"
)

type formulaHandler struct {
	FormulaUsecase domain.FormulaUsecaseInterface
	Responses      domain.ResponseLibraryInterface
}

func NewFormulaHandler(
	formulaUsecase domain.FormulaUsecaseInterface,
	responses domain.ResponseLibraryInterface,
) domain.FormulaHandlerInterface {
	return &formulaHandler{
		FormulaUsecase: formulaUsecase,
		Responses:      responses,
	}
}

func (h *formulaHandler) GetCorp(c *gin.Context) {
	companys, err := h.FormulaUsecase.GetCompany(model.CompanyFilterModel{})
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(companys))
}

func (h *formulaHandler) GetBranch(c *gin.Context) {
	filter := model.BranchFilterModel{}
	if corpID := c.Query("company_id"); corpID != "" {
		corpID = util.QueryDecode(corpID)
		filter.CorpID = corpID
	}
	branchs, err := h.FormulaUsecase.GetBranch(filter)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(branchs))
}

func (h *formulaHandler) GetBook(c *gin.Context) {
	filter := model.BookFilterModel{}
	if corpID := c.Query("company_id"); corpID != "" {
		corpID = util.QueryDecode(corpID)
		filter.CorpID = corpID
	}
	if branchID := c.Query("branch_id"); branchID != "" {
		branchID = util.QueryDecode(branchID)
		filter.BranchID = branchID
	}
	if bookType := c.Query("type"); bookType != "" {
		filter.Type = bookType
	}

	if bookID := c.Query("book_id"); bookID != "" {
		filter.BookID = bookID
	}

	branchs, err := h.FormulaUsecase.Getbook(filter)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(branchs))
}
