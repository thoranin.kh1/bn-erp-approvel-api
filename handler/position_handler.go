package handler

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
)

type positionHandler struct {
	PositionUsecase domain.PositionUsecaseInterface
	Responses       domain.ResponseLibraryInterface
}

func NewPositionHandler(
	positionUsecase domain.PositionUsecaseInterface,
	responses domain.ResponseLibraryInterface,
) domain.PositionHandlerInterface {
	return &positionHandler{
		PositionUsecase: positionUsecase,
		Responses:       responses,
	}
}

func (h *positionHandler) GetPosition(c *gin.Context) {
	approvals, err := h.PositionUsecase.GetPositon()
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approvals))
}

func (h *positionHandler) GetPositionByID(c *gin.Context) {
	id := c.Param("id")
	positionID, _ := strconv.Atoi(id)
	approval, err := h.PositionUsecase.GetPositonByID(positionID)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approval))
}

func (h *positionHandler) CreatePosition(c *gin.Context) {
	request := model.PositionAction{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	approval, err := h.PositionUsecase.CreatePosition(request)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approval))
}

func (h *positionHandler) UpdatePosition(c *gin.Context) {
	request := model.PositionAction{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	id := c.Param("id")
	positionID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}
	request.ID = positionID

	approval, err := h.PositionUsecase.UpdatePosition(request)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(approval))
}

func (h *positionHandler) DeletePosition(c *gin.Context) {
	id := c.Param("id")
	positionID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}
	err = h.PositionUsecase.DeletePosition(positionID)
	if err != nil {
		if errors.Is(err, error_enum.USED.Error()) {
			c.JSON(h.Responses.GetResponseCustom(420, false, "position is used."))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccess())
}
