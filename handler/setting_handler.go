package handler

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"bn-erp-approvel-api/util"
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
)

type settingHandler struct {
	SettingUsecase domain.SettingUsecaseInterface
	Responses      domain.ResponseLibraryInterface
}

func NewSettingHandler(
	settingUsecase domain.SettingUsecaseInterface,
	responses domain.ResponseLibraryInterface,

) domain.SettingHandlerInterface {
	return &settingHandler{
		SettingUsecase: settingUsecase,
		Responses:      responses,
	}
}

func (h *settingHandler) GetDocumentType(c *gin.Context) {
	documentType, err := h.SettingUsecase.GetDocumentType()
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(documentType))
}

func (h *settingHandler) GetApprovalSetting(c *gin.Context) {
	filter := model.SettingFilter{}
	if corpID := c.Query("company_id"); corpID != "" {
		corpID = util.QueryDecode(corpID)
		filter.CorpID = corpID
	}
	if branchID := c.Query("branch_id"); branchID != "" {
		branchID = util.QueryDecode(branchID)
		filter.BranchID = branchID
	}
	if bookType := c.Query("type"); bookType != "" {
		filter.TypeCode = bookType
	}
	settings, err := h.SettingUsecase.GetSetting(filter)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(settings))
}

func (h *settingHandler) GetApprovalSettingByID(c *gin.Context) {
	id := c.Param("id")
	settingID, _ := strconv.Atoi(id)

	settings, err := h.SettingUsecase.GetSettingByID(uint(settingID))
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(settings))
}

func (h *settingHandler) CreateApprovalSetting(c *gin.Context) {
	request := model.SettingRequest{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	request.CorpID = util.QueryDecode(request.CorpID)
	request.BranchID = util.QueryDecode(request.BranchID)
	if request.BookID != "ALL" {
		request.BookID = util.QueryDecode(request.BookID)
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	setting, err := h.SettingUsecase.CreateSetting(request)
	if err != nil {
		if errors.Is(err, error_enum.EXISTS.Error()) {
			c.JSON(h.Responses.GetResponseCustom(431, false, "already exists."))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}

	c.JSON(h.Responses.GetResponseSuccessWithData(setting))
}

func (h *settingHandler) UpdateApprovalSetting(c *gin.Context) {
	id := c.Param("id")
	settingID, _ := strconv.Atoi(id)

	request := model.SettingRequest{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if request.BookID != "ALL" {
		request.BookID = util.QueryDecode(request.BookID)
	}

	request.ID = uint(settingID)

	setting, err := h.SettingUsecase.UpdateSetting(request)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}

	c.JSON(h.Responses.GetResponseSuccessWithData(setting))
}

func (h *settingHandler) DeleteApprovalSetting(c *gin.Context) {
	id := c.Param("id")
	settingID, _ := strconv.Atoi(id)

	err := h.SettingUsecase.DeleteSetting(uint(settingID))
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}

	c.JSON(h.Responses.GetResponseSuccess())
}
