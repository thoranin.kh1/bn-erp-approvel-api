package handler

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/enum/token_enums"
	"bn-erp-approvel-api/model"
	"errors"
	"strconv"

	"github.com/gin-gonic/gin"
)

type userHandler struct {
	UserUsecase domain.UserUsecaseInterface
	Responses   domain.ResponseLibraryInterface
}

func NewUserHandler(
	userUsecase domain.UserUsecaseInterface,
	response domain.ResponseLibraryInterface,
) domain.UserHandlerInterface {
	return &userHandler{
		UserUsecase: userUsecase,
		Responses:   response,
	}
}

func (h *userHandler) SignIn(c *gin.Context) {
	request := model.UserSignin{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	token, err := h.UserUsecase.SignIn(request)
	if err != nil {
		if errors.Is(err, error_enum.PASSWORD.Error()) || errors.Is(err, error_enum.EXPTY.Error()) {
			c.JSON(h.Responses.GetResponseCustomWithData(430, false, "username or password is invalid.", token))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}

	c.JSON(h.Responses.GetResponseSuccessWithData(token))
}

func (h *userHandler) SignOut(c *gin.Context) {
	token := c.GetString(token_enums.TOKEN.String())
	err := h.UserUsecase.DeleteToken(token)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccess())
}

func (h *userHandler) GetUser(c *gin.Context) {
	users, err := h.UserUsecase.GetUser()
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(users))
}

func (h *userHandler) GetUserByID(c *gin.Context) {
	id := c.GetString(token_enums.USER_ID.String())
	if idParam := c.Param("id"); idParam != "" {
		id = idParam
	}

	userID, _ := strconv.Atoi(id)

	user, err := h.UserUsecase.GetUserByID(uint(userID))
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(user))
}

func (h *userHandler) CreateUser(c *gin.Context) {
	request := model.UserCreate{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if match := request.Password == request.ConfirmPassword; !match {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	user, err := h.UserUsecase.CreateUser(request)
	if err != nil {
		if errors.Is(err, error_enum.EXISTS.Error()) {
			c.JSON(h.Responses.GetResponseCustom(433, false, "Username already exists."))
			return
		}
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(user))
}

func (h *userHandler) UpdateUser(c *gin.Context) {
	id := c.Param("id")
	request := model.UserUpdate{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	userID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	request.ID = uint(userID)

	user, err := h.UserUsecase.UpdateUser(request)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}

	c.JSON(h.Responses.GetResponseSuccessWithData(user))
}

func (h *userHandler) UpdatePasswordUser(c *gin.Context) {
	id := c.Param("id")
	request := model.UserPasswordUpdate{}
	if err := c.BindJSON(&request); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	userID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if err := request.Validate(); err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	if match := request.Password == request.ConfirmPassword; !match {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}

	request.ID = uint(userID)

	err = h.UserUsecase.UpdateUserPasword(request)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}

	c.JSON(h.Responses.GetResponseSuccess())
}

func (h *userHandler) DeleteUser(c *gin.Context) {
	id := c.Param("id")
	userID, err := strconv.Atoi(id)
	if err != nil {
		c.JSON(h.Responses.GetResponseBadRequest())
		return
	}
	err = h.UserUsecase.DeleteUser(uint(userID))
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}

	c.JSON(h.Responses.GetResponseSuccess())
}

func (h *userHandler) GetMenu(c *gin.Context) {
	users, err := h.UserUsecase.GetMenu()
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(users))
}

func (h *userHandler) GetUserMenu(c *gin.Context) {
	id := c.GetString(token_enums.USER_ID.String())
	if idParam := c.Param("id"); idParam != "" {
		id = idParam
	}

	userID, _ := strconv.Atoi(id)

	user, err := h.UserUsecase.GetUserPermission(userID)
	if err != nil {
		c.JSON(h.Responses.GetResponseInternalServerError())
		return
	}
	c.JSON(h.Responses.GetResponseSuccessWithData(user))
}
