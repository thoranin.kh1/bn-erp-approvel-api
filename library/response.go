package library

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
	"net/http"
)

type responseLibrary struct{}

func NewResposeLibrary() domain.ResponseLibraryInterface {
	return &responseLibrary{}
}

func (l *responseLibrary) MapResponse(code int, status bool, message string, data interface{}) model.ResponseModel {
	return model.ResponseModel{
		Response: code,
		Status:   status,
		Message:  message,
		Data:     data,
	}
}

func (l *responseLibrary) GetResponseSuccess() (int, model.ResponseModel) {
	return http.StatusOK, l.MapResponse(http.StatusOK, true, "Success.", nil)
}

func (l *responseLibrary) GetResponseSuccessWithData(data interface{}) (int, model.ResponseModel) {
	return http.StatusOK, l.MapResponse(http.StatusOK, true, "Success.", data)
}

func (l *responseLibrary) GetResponseCustom(code int, status bool, message string) (int, model.ResponseModel) {
	return code, l.MapResponse(code, status, message, nil)
}

func (l *responseLibrary) GetResponseCustomWithData(code int, status bool, message string, data interface{}) (int, model.ResponseModel) {
	return code, l.MapResponse(code, status, message, data)
}

func (l *responseLibrary) GetResponseBadRequest() (int, model.ResponseModel) {
	return http.StatusBadRequest, l.MapResponse(http.StatusBadRequest, false, "Bad request.", nil)
}

func (l *responseLibrary) GetResponseUnauthorized() (int, model.ResponseModel) {
	return http.StatusUnauthorized, l.MapResponse(http.StatusUnauthorized, false, "Unauthorized.", nil)
}

func (l *responseLibrary) GetResponseInternalServerError() (int, model.ResponseModel) {
	return http.StatusInternalServerError, l.MapResponse(http.StatusInternalServerError, true, "Internal server error.", nil)
}
