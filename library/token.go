package library

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"fmt"

	"github.com/dgrijalva/jwt-go"
)

type tokenLibrary struct {
	Signature string
}

func NewTokenLibrary(signature string) domain.TokenLibraryInterface {
	return &tokenLibrary{
		Signature: signature,
	}
}

func (l *tokenLibrary) CreateToken(claims model.TokenClaimsModel) (string, error) {
	at := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	token, err := at.SignedString([]byte(l.Signature))
	if err != nil {
		return "", fmt.Errorf("%w : %s", error_enum.TOKEN.Error(), err)
	}
	return token, nil
}

func (l *tokenLibrary) ValidateToken(encyptToken string) (model.TokenClaimsModel, error) {
	claims := model.TokenClaimsModel{}
	token, err := jwt.ParseWithClaims(encyptToken, &claims, func(t *jwt.Token) (interface{}, error) {
		return []byte(l.Signature), nil
	})
	if err != nil {
		return claims, fmt.Errorf("%w : %e", error_enum.TOKEN.Error(), err)
	}
	claims = *token.Claims.(*model.TokenClaimsModel)
	return claims, nil
}
