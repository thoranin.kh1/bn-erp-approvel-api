package main

import (
	"bn-erp-approvel-api/handler"
	"bn-erp-approvel-api/library"
	"bn-erp-approvel-api/middleware"
	"bn-erp-approvel-api/model"
	"bn-erp-approvel-api/repository"
	"bn-erp-approvel-api/usecase"
	"fmt"
	"os"
	"time"

	"github.com/gin-gonic/gin"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

func main() {

	// path = "/Users/thoraninkhumpiphop/go/src/bn-erp-approvel-api"

	// license
	document_types := []string{"PR", "PO"}

	// Config
	servicePort := os.Getenv("APP_PORT")
	rootPassword := os.Getenv("APP_ROOTPASS")
	dsn := os.Getenv("DATABASE_DSN")
	dsnFormula := os.Getenv("DATABASE_FOMULA_DSN")
	approveID := os.Getenv("APP_APPROVE_ID")

	// MARK : Library
	environment := library.NewEnvironment()
	timezone := "Asia/Bangkok"
	err := environment.SetTimezone(timezone)
	if err != nil {
		environment.SetTimezone(time.Local.String())
	}
	token := library.NewTokenLibrary("eyJhbGciOiJIUzI1NiJ9.eyJSb2xlIjoiQWRtaW4iLCJJc3N1ZXIiOiJJc3N1ZXIiLCJVc2VybmFtZSI6IkphdmFJblVzZSIsImV4cCI6MTY1OTk4OTUyNCwiaWF0IjoxNjU5OTg5NTI0fQ.Gb6wOT5RuKKvY8ddkUEGiC7qAu4LAjX5jjTySTy8ZWs")
	response := library.NewResposeLibrary()

	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{
		NowFunc: environment.Now,
	})
	if err != nil {
		fmt.Println(err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	dbFormula, err := gorm.Open(sqlserver.Open(dsnFormula), &gorm.Config{
		NowFunc: environment.Now,
	})
	if err != nil {
		fmt.Println(err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	db.AutoMigrate(&model.User{})
	db.AutoMigrate(&model.Permission{})
	db.AutoMigrate(&model.Menu{})
	db.AutoMigrate(&model.Approval{})
	db.AutoMigrate(&model.Token{})
	db.AutoMigrate(&model.Position{})
	db.AutoMigrate(&model.Setting{})
	db.AutoMigrate(&model.ProcessCommand{})
	db.AutoMigrate(&model.DocumentApprove{})
	db.AutoMigrate(&model.DocumentApproveList{})
	db.AutoMigrate(&model.DocumentInformation{})

	approvalRepository := repository.NewApproveRepository(db)
	positionRepository := repository.NewPositionRepository(db)
	userRepository := repository.NewUserRepository(db)
	formulaRepository := repository.NewFormulaRepository(dbFormula)
	settingRepository := repository.NewSettingRepository(document_types, db)
	documentRepository := repository.NewDocumentRepository(db, dbFormula, approveID)

	approvalUsecase := usecase.NewApproveUsecase(approvalRepository)
	positionUsecase := usecase.NewpositonUsecase(positionRepository)
	userUsecase := usecase.NewUserUsecase(userRepository, token, rootPassword)
	formulaUsecase := usecase.NewFormulaUsecase(formulaRepository)
	settingUsecase := usecase.NewSettingUsecase(settingRepository, positionUsecase, formulaUsecase)
	documentUsecase := usecase.NewDocumentUsecase(documentRepository, settingUsecase, approvalUsecase, userUsecase, environment)

	approveHandler := handler.NewApproveHandler(approvalUsecase, response)
	positionHandler := handler.NewPositionHandler(positionUsecase, response)
	userHandler := handler.NewUserHandler(userUsecase, response)
	formularHandler := handler.NewFormulaHandler(formulaUsecase, response)
	settingHandler := handler.NewSettingHandler(settingUsecase, response)
	documentHandler := handler.NewDocumentHandler(documentUsecase, response)

	err = userUsecase.InitMenu()
	if err != nil {
		fmt.Println("InitMenu", err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	policy := middleware.NewMidleware(userUsecase, token, response)

	router := gin.Default()
	router.Use(policy.CORSMiddleware())
	router.Use(policy.SetJsonResponse())
	router.Use(policy.CORSMiddleware())

	router.GET("/", func(ctx *gin.Context) {
		ctx.JSON(200, model.HelthcheckModel{
			Service: "Approval API",
			Version: "1.0.0-rc",
			Now:     environment.Now().Format(time.RFC3339),
		})
	})

	v1 := router.Group("v1")
	data := v1.Group("/data").Use(policy.JwtAuthMiddleware())
	{
		data.GET("/corp", formularHandler.GetCorp)
		data.GET("/branch", formularHandler.GetBranch)
		data.GET("/book", formularHandler.GetBook)
		data.GET("/document_type", settingHandler.GetDocumentType)
	}

	v1.POST("/auth", userHandler.SignIn)
	profile := v1.Group("/profile").Use(policy.JwtAuthMiddleware())
	{
		profile.GET("/", userHandler.GetUserByID)
		profile.GET("/menu", userHandler.GetUserMenu)
		profile.DELETE("/signout", userHandler.SignOut)
	}

	position := v1.Group("/position").Use(policy.JwtAuthMiddleware())
	{
		position.GET("/", positionHandler.GetPosition)
		position.GET("/:id", positionHandler.GetPositionByID)
		position.POST("/", positionHandler.CreatePosition)
		position.PUT("/:id", positionHandler.UpdatePosition)
		position.DELETE("/:id", positionHandler.DeletePosition)
	}

	setting := v1.Group("/setting").Use(policy.JwtAuthMiddleware())
	{
		setting.GET("/document_type", settingHandler.GetDocumentType)
		setting.GET("/", settingHandler.GetApprovalSetting)
		setting.GET("/:id", settingHandler.GetApprovalSettingByID)
		setting.POST("/", settingHandler.CreateApprovalSetting)
		setting.PUT("/:id", settingHandler.UpdateApprovalSetting)
		setting.DELETE("/:id", settingHandler.DeleteApprovalSetting)
	}

	approval := v1.Group("/approval").Use(policy.JwtAuthMiddleware())
	{
		approval.GET("/", approveHandler.GetApprove)
		approval.GET("/:id", approveHandler.GetApproveByID)
		approval.POST("/", approveHandler.CreateApprove)
		approval.PUT("/:id", approveHandler.UpdateApprove)
		approval.DELETE("/:id", approveHandler.DeleteApprove)
	}

	user := v1.Group("/user").Use(policy.JwtAuthMiddleware())
	{
		user.GET("/", userHandler.GetUser)
		user.GET("/:id", userHandler.GetUserByID)
		user.GET("/menu", userHandler.GetMenu)
		user.GET("/permission/:id", userHandler.GetUserMenu)
		user.POST("/", userHandler.CreateUser)
		user.PUT("/:id", userHandler.UpdateUser)
		user.PUT("/password/:id", userHandler.UpdatePasswordUser)
		user.DELETE("/:id", userHandler.DeleteUser)
	}

	Document := v1.Group("document").Use(policy.JwtAuthMiddleware())
	{
		Document.GET("/", documentHandler.GetDocument)
		Document.GET("/information", documentHandler.GetInformation)
		Document.PUT("/information", documentHandler.ReptyInformationDocument)
	}

	publicDocument := v1.Group("mail")
	{
		publicDocument.GET("/approve", documentHandler.ApproveDocument)
		publicDocument.GET("/reject", documentHandler.RejectDocument)
		publicDocument.PUT("/information", documentHandler.InformationDocument)
	}

	router.Run(servicePort)
}
