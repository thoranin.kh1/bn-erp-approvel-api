package middleware

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/token_enums"
	"fmt"
	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
)

type middleware struct {
	UserUsecase domain.UserUsecaseInterface
	Token       domain.TokenLibraryInterface
	Responses   domain.ResponseLibraryInterface
}

func NewMidleware(
	userUsecase domain.UserUsecaseInterface,
	token domain.TokenLibraryInterface,
	responses domain.ResponseLibraryInterface,
) domain.MiddlewareInterface {
	return &middleware{
		UserUsecase: userUsecase,
		Token:       token,
		Responses:   responses,
	}
}

func (m *middleware) SetJsonResponse() gin.HandlerFunc {
	return func(c *gin.Context) {
		c.Header("Content-Type", "application/json")
		c.Next()
	}
}

func (m *middleware) CORSMiddleware() gin.HandlerFunc {
	return cors.New(cors.Config{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{
			http.MethodGet,
			http.MethodPatch,
			http.MethodPost,
			http.MethodPut,
			http.MethodHead,
			http.MethodDelete,
			http.MethodOptions,
		},
		AllowHeaders: []string{
			"Content-Type",
			"X-XSRF-TOKEN",
			"Accept",
			"Origin",
			"X-Requested-With",
			"Authorization",
			"Device",
			"access-control-allow-origin, access-control-allow-headers",
		},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		AllowFiles:       true,
	})
}

func (m *middleware) JwtAuthMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		const BEARER_SCHEMA = "Bearer "
		authHeader := c.GetHeader("Authorization")
		if authHeader == "" {
			c.AbortWithStatusJSON(m.Responses.GetResponseUnauthorized())
			return
		}
		tokenString := authHeader[len(BEARER_SCHEMA):]
		claims, err := m.Token.ValidateToken(tokenString)
		if err != nil {
			c.AbortWithStatusJSON(m.Responses.GetResponseUnauthorized())
			return
		}
		hasToken, _ := m.UserUsecase.HasToken(tokenString)
		if !hasToken {
			c.AbortWithStatusJSON(m.Responses.GetResponseUnauthorized())
			return
		}

		c.Set(token_enums.USER_ID.String(), fmt.Sprintf("%d", claims.ID))
		c.Set(token_enums.TOKEN.String(), tokenString)
		c.Next()
	}
}
