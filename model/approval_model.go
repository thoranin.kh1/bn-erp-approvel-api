package model

import (
	"time"

	"gopkg.in/validator.v2"
	"gorm.io/gorm"
)

type ApprovalFilter struct {
	ID         int
	PositionID uint
}

type Approval struct {
	ID           uint           `gorm:"primaryKey" json:"id"`
	FirstName    string         `json:"first_name"`
	LastName     string         `json:"last_name"`
	Email        string         `json:"email"`
	Remark       string         `json:"remark"`
	PositionID   uint           `json:"position_id"`
	PositionName string         `json:"position_name"`
	CreatedAt    time.Time      `json:"-"`
	UpdatedAt    time.Time      `json:"-"`
	DeletedAt    gorm.DeletedAt `gorm:"index" json:"-"`
}

type ApprovalAction struct {
	ID        int
	FirstName string `json:"first_name" validate:"nonzero"`
	LastName  string `json:"last_name"`
	Remark    string `json:"remark"`
	Email     string `json:"email" validate:"nonzero"`
}

func (u *ApprovalAction) Validate() error {
	if errs := validator.Validate(u); errs != nil {
		return errs
	}
	return nil
}
