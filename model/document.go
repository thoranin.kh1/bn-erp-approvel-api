package model

import (
	"bn-erp-approvel-api/util"
	"database/sql"
	"strings"
	"time"

	"gorm.io/gorm"
)

type DocumentFilter struct {
	ID             int    `json:"id"`
	TypeCode       string `json:"type_code"`
	RefID          string `json:"ref_id"`
	BookID         string `json:"book_id"`
	DocumentStatus string `json:"document_status"`
	DateStart      string `json:"create_date_start"`
	DateEnd        string `json:"create_date_end"`
	CorpID         string `json:"corp_id"`
	BranchID       string `json:"branch_id"`
}

type DocumentApprove struct {
	ID                  uint           `gorm:"primaryKey" json:"id"`
	DocumentRefID       string         `json:"-"`
	DocumentCode        string         `json:"document_code"`
	DocumentDate        time.Time      `json:"document_date"`
	DocumentRefNo       string         `json:"document_ref_no"`
	SettingID           string         `json:"-"`
	TypeCode            string         `json:"type_code"`
	BookID              string         `json:"book_id"`
	BookCode            string         `json:"book_code"`
	ApprovalID01        uint           `json:"approval_id_01"`
	ApprovalFirstName01 string         `json:"approval_first_name_01"`
	ApprovalLastName01  string         `json:"approval_last_name_01"`
	ApprovalStatus01    string         `json:"approval_status_01"`
	ApprovalDate01      time.Time      `json:"approval_date_01"`
	PositionID01        uint           `json:"position_id_01"`
	PositionName01      string         `json:"position_name_01"`
	ApprovalID02        uint           `json:"approval_id_02"`
	ApprovalFirstName02 string         `json:"approval_first_name_02"`
	ApprovalLastName02  string         `json:"approval_last_name_02"`
	ApprovalStatus02    string         `json:"approval_status_02"`
	ApprovalDate02      time.Time      `json:"approval_date_02"`
	PositionID02        uint           `json:"position_id_02"`
	PositionName02      string         `json:"position_name_02"`
	ApprovalID03        uint           `json:"approval_id_03"`
	ApprovalFirstName03 string         `json:"approval_first_name_03"`
	ApprovalLastName03  string         `json:"approval_last_name_03"`
	ApprovalStatus03    string         `json:"approval_status_03"`
	ApprovalDate03      time.Time      `json:"approval_date_03"`
	PositionID03        uint           `json:"position_id_03"`
	PositionName03      string         `json:"position_name_03"`
	ApprovalID04        uint           `json:"approval_id_04"`
	ApprovalFirstName04 string         `json:"approval_first_name_04"`
	ApprovalLastName04  string         `json:"approval_last_name_04"`
	ApprovalStatus04    string         `json:"approval_status_04"`
	ApprovalDate04      time.Time      `json:"approval_date_04"`
	PositionID04        uint           `json:"position_id_04"`
	PositionName04      string         `json:"position_name_04"`
	ApprovalID05        uint           `json:"approval_id_05"`
	ApprovalFirstName05 string         `json:"approval_first_name_05"`
	ApprovalLastName05  string         `json:"approval_last_name_05"`
	ApprovalStatus05    string         `json:"approval_status_05"`
	ApprovalDate05      time.Time      `json:"approval_date_05"`
	PositionID05        uint           `json:"position_id_05"`
	PositionName05      string         `json:"position_name_05"`
	StepCondition       int            `json:"step_condition"`
	ApproveConditon     int            `json:"approval_condition"`
	DocumentStatus      string         `json:"document_status"`
	CorpID              string         `json:"-"`
	BranchID            string         `json:"-"`
	CorpIDb64           string         `json:"corp_id"`
	BranchIDb64         string         `json:"branch_id"`
	CreatedAt           time.Time      `json:"-"`
	UpdatedAt           time.Time      `json:"-"`
	DeletedAt           gorm.DeletedAt `gorm:"index" json:"-"`
}

func (m *DocumentApprove) AfterFind(*gorm.DB) error {
	m.CorpIDb64 = util.QueryEncode(strings.TrimSpace(m.CorpID))
	m.BranchIDb64 = util.QueryEncode(strings.TrimSpace(m.BranchID))
	return nil
}

type DocumentApproveList struct {
	ID                uint           `gorm:"primaryKey" json:"id"`
	DocumentID        uint           `json:"document_id"`
	ApprovalID        uint           `json:"approval_id"`
	ApprovalFirstName string         `json:"approval_first_name"`
	ApprovalLastName  string         `json:"approval_last_name"`
	ApprovalEmail     string         `json:"approval_email"`
	ApprovalLevel     int            `json:"approval_level"`
	PositionID        uint           `json:"position_id"`
	PositionName      string         `json:"position_name"`
	TransactionKey    string         `json:"-"`
	CreatedAt         time.Time      `json:"-"`
	UpdatedAt         time.Time      `json:"-"`
	DeletedAt         gorm.DeletedAt `gorm:"index" json:"-"`
}

type FilterInformation struct {
	DocumentID uint `json:"document_id"`
}

type DocumentInformation struct {
	ID                uint           `gorm:"primaryKey" json:"id"`
	DocumentID        uint           `json:"document_id"`
	ApprovalID        uint           `json:"approval_id"`
	ApprovalFirstName string         `json:"approval_first_name"`
	ApprovalLastName  string         `json:"approval_last_name"`
	InformationType   string         `json:"information_type"`
	Message           string         `json:"message"`
	Sequence          int            `json:"sequence"`
	CreatedAt         time.Time      `json:"-"`
	UpdatedAt         time.Time      `json:"-"`
	DeletedAt         gorm.DeletedAt `gorm:"index" json:"-"`
}

type RequestDocumentInformation struct {
	Key     string `json:"key"`
	Message string `json:"message"`
}

type ReptyDocumentInformation struct {
	ID      int    `json:"id"`
	UserID  int    `json:"-"`
	Message string `json:"message"`
}

type PrTemplate struct {
	SENT             string
	CORP             string
	BRANCH           string
	DOCUMENT_CODE    string
	DOCUMENT_DATE    string
	BOOK_CODE        string
	REF_NO           string
	SUPP_CODE        string
	SUPP_NAME        string
	SUB_TOTAL        string
	DIS              string
	VAT              string
	TOTAL            string
	APPROVE_LINK     string
	REJECRT_LINK     string
	INFORMATION_LINK string
	IS_INFORMATION   bool
	CREATE_BY        string
	SECTION_NAME     string
	PROJECT_NAME     string
	INFORMATION_DATA []InformationTemplate
	ITEMS            []PrItemTemplate
}

type InformationTemplate struct {
	INFORMATION_TYPE string
	MESSAGE          string
}

type PrItemTemplate struct {
	NO        string
	ITEM_CODE string
	ITEM_NAME string
	PRICE     string
	QTY       string
	AMOUNT    string
}

type DataDocument struct {
	DocumentCode  string       `gorm:"column:DOC_CODE"`
	DocumentRefNo string       `gorm:"column:FCREFNO"`
	DocumentDate  sql.NullTime `gorm:"column:FDDATE"`
	BookCode      string       `gorm:"column:BOOK_CODE"`
	BookName      string       `gorm:"column:BOOK_NAME"`
	ItemCode      string       `gorm:"column:PD_CODE"`
	ItemName      string       `gorm:"column:PD_NAME"`
	Price         float64      `gorm:"column:FNPRICE"`
	Qty           float64      `gorm:"column:FNQTY"`
	SuppCode      string       `gorm:"column:SUB_CODE"`
	SuppName      string       `gorm:"column:SUB_NAME"`
	CorpName      string       `gorm:"column:CORP_NAME"`
	BranchName    string       `gorm:"column:BRANCH_NAME"`
	CreateBy      string       `gorm:"column:CREATEBY"`
	SectionName   string       `gorm:"column:SECT_NAME"`
	ProjectName   string       `gorm:"column:PROJ_NAME"`
	Dis           float64      `gorm:"column:DIS"`
	Vat           float64      `gorm:"column:VAT"`
	Total         float64      `gorm:"column:TOTAL"`
}

func (m *DataDocument) AfterFind(*gorm.DB) error {
	m.DocumentCode = strings.TrimSpace(m.DocumentCode)
	m.DocumentRefNo = strings.TrimSpace(m.DocumentRefNo)
	m.ItemCode = strings.TrimSpace(m.ItemCode)
	m.ItemName = strings.TrimSpace(m.ItemName)
	m.SuppCode = strings.TrimSpace(m.SuppCode)
	m.SuppName = strings.TrimSpace(m.SuppName)
	return nil
}

type PBDataDocument struct {
	DocumentCode  string       `gorm:"column:DOC_CODE"`
	DocumentRefNo string       `gorm:"column:REF_NO"`
	DocumentDate  sql.NullTime `gorm:"column:DOC_DATE"`
	SuppCode      string       `gorm:"column:SUPP_CODE"`
	SuppName      string       `gorm:"column:SUPP_NAME"`
	RefType       string       `gorm:"column:FCREFTYPE"`
	BookCode      string       `gorm:"column:BOOK_CODE"`
	GlRefCode     string       `gorm:"column:GLREF_CODE"`
	GlRefRefNo    string       `gorm:"column:GLREF_REFNO"`
	GlRefDueDate  sql.NullTime `gorm:"column:GLREF_DUEDATE"`
	Amount        float64      `gorm:"column:AMT"`
	PayAmount     float64      `gorm:"column:PAYAMT"`
	XRate         float64      `gorm:"column:FNXRATE"`
	VatAmount     float64      `gorm:"column:FNVATAMTKE"`
	PayDate       sql.NullTime `gorm:"column:PAY_DATE"`
	PayCode       string       `gorm:"column:PAY_CODE"`
	PayName       string       `gorm:"column:PAY_NAME"`
	PayRecV2      string       `gorm:"column:FCPAYRECV2"`
	BankCode      string       `gorm:"column:BANK_CODE"`
	BankName      string       `gorm:"column:BANK_NAME"`
	BBranch       string       `gorm:"column:BBRANCH"`
	FnVatAmt      float64      `gorm:"column:FNVATAMT"`
	FnAmt         float64      `gorm:"column:FNAMT"`
	Total         float64      `gorm:"column:TOTAL"`
	ChqAmt        float64      `gorm:"column:CHQ_AMT"`
	CreateBy      string       `gorm:"column:CREATEBY"`
	SectionName   string       `gorm:"column:SECT_NAME"`
	ProjectName   string       `gorm:"column:PROJ_NAME"`
}

type PbTemplate struct {
	SENT             string
	BOOK_CODE        string
	DOCUMENT_CODE    string
	REF_NO           string
	DOCUMENT_DATE    string
	SUPP_CODE        string
	SUPP_NAME        string
	PAY_DATE         string
	PAY_CODE         string
	PAY_NAME         string
	FCPAYRECV2       string
	BANK_CODE        string
	BANK_NAME        string
	CHQ_AMT          string
	FNAMT            string
	FNVATAMT         string
	TOTAL            string
	APPROVE_LINK     string
	REJECRT_LINK     string
	INFORMATION_LINK string
	IS_INFORMATION   bool
	CREATE_BY        string
	SECTION_NAME     string
	PROJECT_NAME     string
	INFORMATION_DATA []InformationTemplate
	ITEMS            []PbItemTemplate
}

type PbItemTemplate struct {
	NO            string
	FCREFTYPE     string
	BOOK_CODE     string
	GLREF_CODE    string
	GLREF_REFNO   string
	GLREF_DUEDATE string
	AMT           string
	PAYAMT        string
	FNXRATE       string
	FNVATAMTKE    string
	FNAMT         string
}
