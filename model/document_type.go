package model

type DocumentType struct {
	Code string `json:"code"`
	Name string `json:"name"`
}
