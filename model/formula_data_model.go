package model

import (
	"bn-erp-approvel-api/util"
	"strings"
	"time"

	"gorm.io/gorm"
)

// MARK :: Company

type CompanyFilterModel struct {
	ID string
}

type CompanyModel struct {
	ID    string `gorm:"column:FCSKID" json:"-"`
	IDB64 string `json:"id"`
	Code  string `gorm:"column:FCCODE" json:"code"`
	Name  string `gorm:"column:FCNAME" json:"name"`
}

func (CompanyModel) TableName() string {
	return "CORP"
}

func (m *CompanyModel) AfterFind(*gorm.DB) error {
	m.ID = strings.TrimSpace(m.ID)
	m.IDB64 = util.QueryEncode(m.ID)
	m.Code = strings.TrimSpace(m.Code)
	m.Name = strings.TrimSpace(m.Name)
	return nil
}

// MARK :: Branch

type BranchFilterModel struct {
	ID     string
	CorpID string
}

type BranchModel struct {
	ID     string `gorm:"column:FCSKID" json:"-"`
	IDB64  string `json:"id"`
	Code   string `gorm:"column:FCCODE" json:"code"`
	Name   string `gorm:"column:FCNAME" json:"name"`
	CorpID string `gorm:"column:FCCORP" json:"-"`
}

func (BranchModel) TableName() string {
	return "BRANCH"
}

func (m *BranchModel) AfterFind(*gorm.DB) error {
	m.ID = strings.TrimSpace(m.ID)
	m.IDB64 = util.QueryEncode(m.ID)
	m.Code = strings.TrimSpace(m.Code)
	m.Name = strings.TrimSpace(m.Name)
	return nil
}

// MARK :: Book
type BookFilterModel struct {
	ID       string
	CorpID   string
	BranchID string
	BookID   string
	Type     string
}

type BookModel struct {
	ID       string `gorm:"column:FCSKID" json:"-"`
	IDB64    string `json:"id"`
	Name     string `gorm:"column:FCNAME" json:"name"`
	Code     string `gorm:"column:FCCODE" json:"code"`
	Type     string `gorm:"column:FCREFTYPE" json:"type"`
	CorpID   string `gorm:"column:FCCORP" json:"-"`
	BranchID string `gorm:"column:FCBRANCH" json:"-"`
}

func (BookModel) TableName() string {
	return "BOOK"
}

func (m *BookModel) AfterFind(*gorm.DB) error {
	m.ID = strings.TrimSpace(m.ID)
	m.IDB64 = util.QueryEncode(m.ID)
	m.Name = strings.TrimSpace(m.Name)
	m.Code = strings.TrimSpace(m.Code)
	m.Type = strings.TrimSpace(m.Type)
	m.CorpID = strings.TrimSpace(m.CorpID)
	m.BranchID = strings.TrimSpace(m.BranchID)
	return nil
}

type PRDocument struct {
	ID               string    `gorm:"column:FCSKID" json:"id"`
	Status           string    `gorm:"column:FCU3STATUS" json:"status"`
	Code             string    `gorm:"column:FCCODE" json:"code"`
	RefNo            string    `gorm:"column:FCREFNO" json:"ref_no"`
	BookID           string    `gorm:"column:BOOK_ID" json:"book_id"`
	BookCode         string    `gorm:"column:BOOK_CODE" json:"book_code"`
	CorpID           string    `gorm:"column:FCCORP" json:"corp_id"`
	BranchID         string    `gorm:"column:FCBRANCH" json:"branch_id"`
	DocumentDateTime time.Time `gorm:"column:FDDATE" json:"-"`
}

type PBDocument struct {
	ID               string    `gorm:"column:FCSKID" json:"id"`
	Status           string    `gorm:"column:FCU3STATUS" json:"status"`
	Code             string    `gorm:"column:FCCODE" json:"code"`
	RefNo            string    `gorm:"column:FCREFNO" json:"ref_no"`
	BookID           string    `gorm:"column:BOOK_ID" json:"book_id"`
	BookCode         string    `gorm:"column:BOOK_CODE" json:"book_code"`
	CorpID           string    `gorm:"column:FCCORP" json:"corp_id"`
	BranchID         string    `gorm:"column:FCBRANCH" json:"branch_id"`
	DocumentDateTime time.Time `gorm:"column:FDDATE" json:"-"`
}

// func (m *PBDocument) AfterFind(*gorm.DB) error {
// 	if m.DocumentDateTime.Valid {

// 	}
// 	return nil
// }
