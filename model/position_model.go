package model

import (
	"time"

	"gopkg.in/validator.v2"
	"gorm.io/gorm"
)

type PositionFilter struct {
	ID int
}

type Position struct {
	ID        uint           `gorm:"primaryKey" json:"id"`
	Name      string         `json:"name"`
	Total     int            `json:"total"`
	Remark    string         `json:"remark"`
	Used      bool           `json:"used"`
	CreatedAt time.Time      `json:"-"`
	UpdatedAt time.Time      `json:"-"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}

type PositionAction struct {
	ID        int
	Name      string `json:"name" validate:"nonzero"`
	Remark    string `json:"remark"`
	Approvals []int  `json:"approvals"`
}

func (u *PositionAction) Validate() error {
	if errs := validator.Validate(u); errs != nil {
		return errs
	}
	return nil
}
