package model

import (
	"time"

	"gorm.io/gorm"
)

type ProcessCommand struct {
	ID            uint `gorm:"primaryKey"`
	RefID         string
	CommandType   string
	CommandAction string
	Payload       string
	Message       string
	CreatedAt     time.Time
	UpdatedAt     time.Time
	DeletedAt     gorm.DeletedAt `gorm:"index"`
}
