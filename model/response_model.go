package model

type ResponseModel struct {
	Response int         `json:"response"`
	Status   bool        `json:"status"`
	Message  string      `json:"message"`
	Data     interface{} `json:"data,omitempty"`
}
