package model

import (
	"bn-erp-approvel-api/util"
	"errors"
	"time"

	"gopkg.in/validator.v2"
	"gorm.io/gorm"
)

type SettingType struct {
	ID        uint           `gorm:"primaryKey" json:"id"`
	TypeCode  string         `json:"type_code"`
	Name      string         `json:"-"`
	CreatedAt time.Time      `json:"-"`
	UpdatedAt time.Time      `json:"-"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}

type SettingFilter struct {
	ID       uint
	TypeCode string `json:"type_code"`
	BookID   string `json:"book_id"`
	BookCode string `json:"book_code"`
	CorpID   string `json:"corp_id"`
	BranchID string `json:"barnch_id"`
}

type Setting struct {
	ID                     uint           `gorm:"primaryKey" json:"id"`
	Name                   string         `json:"name"`
	TypeCode               string         `json:"type_code"`
	BookID                 string         `json:"-"`
	BookIDB64              string         `json:"book_id"`
	BookCode               string         `json:"book_code"`
	BookName               string         `json:"book_name"`
	ApprovalPositionID01   uint           `json:"approval_position_id_01"`
	ApprovalPositionName01 string         `json:"approval_position_name_01"`
	ApprovalPositionID02   uint           `json:"approval_position_id_02"`
	ApprovalPositionName02 string         `json:"approval_position_name_02"`
	ApprovalPositionID03   uint           `json:"approval_position_id_03"`
	ApprovalPositionName03 string         `json:"approval_position_name_03"`
	ApprovalPositionID04   uint           `json:"approval_position_id_04"`
	ApprovalPositionName04 string         `json:"approval_position_name_04"`
	ApprovalPositionID05   uint           `json:"approval_position_id_05"`
	ApprovalPositionName05 string         `json:"approval_position_name_05"`
	ApproveConditon        int            `json:"approval_condition"`
	CorpID                 string         `json:"-"`
	BranchID               string         `json:"-"`
	CorpIDb64              string         `json:"corp_id"`
	BranchIDb64            string         `json:"branch_id"`
	CreatedAt              time.Time      `json:"-"`
	UpdatedAt              time.Time      `json:"-"`
	DeletedAt              gorm.DeletedAt `gorm:"index" json:"-"`
}

func (m *Setting) AfterFind(*gorm.DB) error {
	if m.BookID == "ALL" {
		m.BookCode = "-"
		m.BookName = "ทั้งหมด"
	} else {
		m.BookIDB64 = util.QueryEncode(m.BookID)
		m.CorpIDb64 = util.QueryEncode(m.CorpID)
		m.BranchIDb64 = util.QueryEncode(m.BranchID)
	}

	return nil
}

type SettingRequest struct {
	ID           uint
	Name         string `json:"name" validate:"nonzero"`
	BookID       string `json:"book_id" validate:"nonzero"`
	Type         string `json:"type" validate:"nonzero"`
	PositionID01 uint   `json:"position_id_01"`
	PositionID02 uint   `json:"position_id_02"`
	PositionID03 uint   `json:"position_id_03"`
	PositionID04 uint   `json:"position_id_04"`
	PositionID05 uint   `json:"position_id_05"`
	CorpID       string `json:"company_id" validate:"nonzero"`
	BranchID     string `json:"branch_id" validate:"nonzero"`
}

func (u *SettingRequest) Validate() error {
	if errs := validator.Validate(u); errs != nil {
		return errs
	}
	positionCheck := (u.PositionID01 + u.PositionID02 + u.PositionID03 + u.PositionID04 + u.PositionID05)
	if positionCheck <= 0 {
		return errors.New("POSITION_NOT_FOUND")
	}
	return nil
}
