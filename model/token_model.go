package model

import (
	"time"

	"github.com/dgrijalva/jwt-go"
	"gorm.io/gorm"
)

type Token struct {
	ID        uint           `gorm:"primaryKey" json:"-"`
	UserID    uint           `json:"-"`
	Token     string         `json:"token"`
	CreatedAt time.Time      `json:"-"`
	UpdatedAt time.Time      `json:"-"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}

func (Token) TableName() string {
	return "tokens"
}

type TokenClaimsModel struct {
	jwt.StandardClaims
	ID        uint
	UserName  string
	Email     string
	FirstName string
	LastName  string
	CreateAt  time.Time
}
