package model

import (
	"time"

	"gopkg.in/validator.v2"
	"gorm.io/gorm"
)

type UserFilter struct {
	ID       uint
	Username string
}

type User struct {
	ID        uint           `gorm:"primaryKey" json:"id"`
	Username  string         `json:"username"`
	Password  string         `json:"-"`
	FirstName string         `json:"first_name"`
	LastName  string         `json:"last_name"`
	CreatedAt time.Time      `json:"-"`
	UpdatedAt time.Time      `json:"-"`
	DeletedAt gorm.DeletedAt `gorm:"index" json:"-"`
}

type UserCreate struct {
	Username        string   `json:"username"  validate:"nonzero, regexp=^[a-zA-Z0-9]*$"`
	Password        string   `json:"password"  validate:"nonzero"`
	ConfirmPassword string   `json:"confirm_password"  validate:"nonzero"`
	FirstName       string   `json:"first_name"  validate:"nonzero"`
	LastName        string   `json:"last_name"`
	Permissions     []string `json:"permissions"`
}

func (u *UserCreate) Validate() error {
	if errs := validator.Validate(u); errs != nil {
		return errs
	}
	return nil
}

type UserUpdate struct {
	ID          uint
	FirstName   string   `json:"first_name" validate:"nonzero"`
	LastName    string   `json:"last_name"`
	Permissions []string `json:"permissions"`
}

func (u *UserUpdate) Validate() error {
	if errs := validator.Validate(u); errs != nil {
		return errs
	}
	return nil
}

type UserPasswordUpdate struct {
	ID              uint
	Password        string `json:"password" validate:"nonzero"`
	ConfirmPassword string `json:"confirm_password" validate:"nonzero"`
}

func (u *UserPasswordUpdate) Validate() error {
	if errs := validator.Validate(u); errs != nil {
		return errs
	}
	return nil
}

type UserSignin struct {
	Username string `json:"username" validate:"nonzero"`
	Password string `json:"password" validate:"nonzero"`
}

func (u *UserSignin) Validate() error {
	if errs := validator.Validate(u); errs != nil {
		return errs
	}
	return nil
}
