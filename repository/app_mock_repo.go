package repository

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
)

type appMock struct{}

func NewApproveMockRepository() domain.ApproveRepositoryInterface {
	return &appMock{}
}

func (u *appMock) Get(filter model.ApprovalFilter) ([]model.Approval, error) {
	data := []model.Approval{}
	data = append(data, model.Approval{
		FirstName: "FirstName",
		LastName:  "LastName",
	})
	return data, nil
}

func (u *appMock) Create(data model.Approval) (model.Approval, error) {
	return model.Approval{}, nil
}

func (u *appMock) Update(data model.Approval) (model.Approval, error) {
	return model.Approval{}, nil
}

func (u *appMock) Delete(id int) error {
	return nil
}
