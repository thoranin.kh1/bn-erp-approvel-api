package repository

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
	"context"
	"errors"

	"gorm.io/gorm"
)

type approveRepository struct {
	AppDB *gorm.DB
}

func NewApproveRepository(appDB *gorm.DB) domain.ApproveRepositoryInterface {
	return &approveRepository{
		AppDB: appDB,
	}
}

func (r *approveRepository) Get(filter model.ApprovalFilter) ([]model.Approval, error) {
	data := []model.Approval{}
	db := r.AppDB.Model(&model.Approval{})
	if filter.ID > 0 {
		db.Where("id = ?", filter.ID)
	}
	if filter.PositionID > 0 {
		db.Where("position_id = ?", filter.PositionID)
	}
	err := db.Find(&data).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return []model.Approval{}, nil
	}
	return data, err
}

func (r *approveRepository) Create(data model.Approval) (model.Approval, error) {
	err := r.AppDB.Create(&data).Error
	if err != nil {
		return model.Approval{}, nil
	}
	return data, err
}

func (r *approveRepository) Update(data model.Approval) (model.Approval, error) {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {

		err := tx.Save(&data).Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.DocumentApprove{}).Where("approval_id01 = ?", data.ID).Update("approval_first_name01", data.FirstName).Update("approval_last_name01", data.LastName).Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.DocumentApprove{}).Where("approval_id02 = ?", data.ID).Update("approval_first_name02", data.FirstName).Update("approval_last_name02", data.LastName).Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.DocumentApprove{}).Where("approval_id03 = ?", data.ID).Update("approval_first_name03", data.FirstName).Update("approval_last_name03", data.LastName).Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.DocumentApprove{}).Where("approval_id04 = ?", data.ID).Update("approval_first_name04", data.FirstName).Update("approval_last_name04", data.LastName).Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.DocumentApprove{}).Where("approval_id05 = ?", data.ID).Update("approval_first_name05", data.FirstName).Update("approval_last_name05", data.LastName).Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.DocumentApproveList{}).Where("approval_id = ?", data.ID).Update("approval_first_name", data.FirstName).Update("approval_last_name", data.LastName).Update("approval_email", data.Email).Error
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return model.Approval{}, err
	}
	return data, err
}

func (r *approveRepository) Delete(id int) error {
	err := r.AppDB.Where("id=?", id).Delete(&model.Approval{}).Error
	if err != nil {
		return err
	}
	return nil
}
