package repository

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/command_action_enums"
	"bn-erp-approvel-api/enum/command_enums"
	"bn-erp-approvel-api/enum/document_status_enums"
	"bn-erp-approvel-api/enum/document_type_enums"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"context"
	"fmt"
	"strconv"

	"gorm.io/gorm"
)

type documentRepository struct {
	AppDB     *gorm.DB
	FormulaDB *gorm.DB
	ApproveID string
}

func NewDocumentRepository(
	appDB *gorm.DB,
	formulaDB *gorm.DB,
	approveID string,
) domain.DocumentRepositoryInterface {
	return &documentRepository{
		AppDB:     appDB,
		FormulaDB: formulaDB,
		ApproveID: approveID,
	}
}

func (r *documentRepository) GetApproveData(key string) (model.DocumentApproveList, error) {
	approve := model.DocumentApproveList{}
	db := r.AppDB.Model(&model.DocumentApproveList{}).Where("transaction_key = ?", key)
	err := db.First(&approve).Error
	if err != nil {
		return model.DocumentApproveList{}, err
	}
	return approve, nil
}

func (r *documentRepository) GetTransectionProcess(id int) (model.ProcessCommand, error) {
	procese := model.ProcessCommand{}
	db := r.AppDB.Model(&model.ProcessCommand{})
	db.Where("id = ?", id)
	err := db.First(&procese).Error
	if err != nil {
		return model.ProcessCommand{}, nil
	}

	return procese, nil
}

func (r *documentRepository) UpdateTransection(transection model.ProcessCommand) error {
	err := r.AppDB.Save(&transection).Error
	return err
}

func (r *documentRepository) GetPRFormulaDocumentData(id string) (model.PRDocument, error) {
	query := `SELECT 
	ORDERH.FCSKID,
	ORDERH.FCU3STATUS,
	ORDERH.FCCODE,
	ORDERH.FCREFNO,
	BOOK.FCSKID AS BOOK_ID,
	BOOK.FCCODE AS BOOK_CODE,
	ORDERH.FCCORP,
	ORDERH.FCBRANCH,
	ORDERH.FDDATE
	FROM ORDERH
	LEFT JOIN BOOK ON ORDERH.FCBOOK = BOOK.FCSKID
	WHERE ORDERH.FCSKID = ? 
	AND ORDERH.FCSTAT <> 'C' `
	docPR := model.PRDocument{}
	params := []interface{}{id}
	err := r.FormulaDB.Raw(query, params...).Scan(&docPR).Error
	if err != nil {
		return model.PRDocument{}, err
	}
	return docPR, nil
}

func (r *documentRepository) GetPBFormulaDocumentData(id string) (model.PBDocument, error) {
	query := `SELECT 
	PRCVH.FCSKID,
	PRCVH.FCCODE,
	PRCVH.FCREFNO,
	BOOK.FCSKID AS BOOK_ID,
	BOOK.FCCODE AS BOOK_CODE,
	PRCVH.FCCORP,
	PRCVH.FCBRANCH,
	PRCVH.FDDATE
	FROM PRCVH 
	LEFT JOIN PRCVI ON  PRCVH.FCSKID = PRCVI.FCPRCVH
	LEFT JOIN BOOK ON PRCVH.FCBOOK = BOOK.FCSKID
	WHERE PRCVH.FCREFTYPE='PB' AND PRCVH.FCSKID = ? `
	doc := model.PBDocument{}
	params := []interface{}{id}
	err := r.FormulaDB.Raw(query, params...).Scan(&doc).Error
	if err != nil {
		return model.PBDocument{}, err
	}
	return doc, nil
}

func (r *documentRepository) GetDocument(filter model.DocumentFilter) ([]model.DocumentApprove, error) {
	documents := []model.DocumentApprove{}
	db := r.AppDB.Model(&model.DocumentApprove{})

	if filter.ID > 0 {
		db.Where("id = ?", filter.ID)
	}

	if filter.TypeCode != "" {
		db.Where("type_code = ?", filter.TypeCode)
	}

	if filter.RefID != "" {
		db.Where("document_ref_id = ?", filter.RefID)
	}

	if filter.BookID != "" {
		db.Where("book_id = ?", filter.BookID)
	}

	if filter.DocumentStatus != "" {
		db.Where("document_status = ?", filter.DocumentStatus)
	}

	if filter.CorpID != "" {
		db.Where("corp_id = ?", filter.CorpID)
	}

	if filter.BranchID != "" {
		db.Where("branch_id = ?", filter.BranchID)
	}

	if filter.DateStart != "" && filter.DateEnd != "" {
		db.Where("document_date BETWEEN ? AND ?", filter.DateStart+" 00:00:00", filter.DateEnd+" 23:59:59")
	}

	err := db.Find(&documents).Error
	if err != nil {
		return []model.DocumentApprove{}, nil
	}
	return documents, nil
}

func (r *documentRepository) CreateDocument(document model.DocumentApprove, approves []model.DocumentApproveList) error {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Save(&document).Error
		if err != nil {
			return err
		}

		for i := range approves {
			approves[i].DocumentID = document.ID
		}

		err = tx.Save(&approves).Error
		if err != nil {
			return err
		}

		err = tx.Create(&model.ProcessCommand{
			RefID:         strconv.Itoa(int(document.ID)),
			CommandType:   command_enums.EMAIL.String(),
			Payload:       "CreateDocument",
			CommandAction: command_action_enums.INSERT.String(),
		}).Error
		if err != nil {
			return err
		}

		return nil
	})
	return err
}

func (r *documentRepository) UpdateDocument(document model.DocumentApprove) error {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Save(&document).Error
		if err != nil {
			return err
		}

		if document.DocumentStatus == document_status_enums.PENDING.String() {
			err = tx.Create(&model.ProcessCommand{
				RefID:         strconv.Itoa(int(document.ID)),
				CommandType:   command_enums.EMAIL.String(),
				Payload:       "UpdateDocument",
				CommandAction: command_action_enums.UPDATE.String(),
			}).Error
			if err != nil {
				return err
			}
		}

		if document.DocumentStatus == document_status_enums.APPROVE.String() {
			switch document.TypeCode {
			case document_type_enums.PO.String():
				err = r.FormulaDB.Exec("UPDATE ORDERH SET FCAPPROVEB=?, FCSTEP='1', FDAPPROVE=GETDATE() WHERE FCSKID=? ",
					r.ApproveID,
					document.DocumentRefID,
				).Error
				if err != nil {
					return err
				}
			case document_type_enums.PR.String():
				err = r.FormulaDB.Exec("UPDATE ORDERH SET FCAPPROVEB=?, FCSTEP='1', FDAPPROVE=GETDATE() WHERE FCSKID=? ",
					r.ApproveID,
					document.DocumentRefID,
				).Error
				if err != nil {
					return err
				}
			case document_type_enums.PB.String():
				err = r.FormulaDB.Exec("UPDATE PRCVH SET FCAPPROVEB=?, FCSTEP='1', FDAPPROVE=GETDATE() WHERE FCSKID=? ",
					r.ApproveID,
					document.DocumentRefID,
				).Error
				if err != nil {
					return err
				}
			}
		}
		return nil
	})

	return err
}

func (r *documentRepository) DeleteDocumentByRefID(refID string) error {
	err := r.AppDB.Where("document_ref_id=?", refID).Delete(&model.DocumentApprove{}).Error
	if err != nil {
		return fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
	}
	return nil
}

func (r *documentRepository) GetInformation(filter model.FilterInformation) ([]model.DocumentInformation, error) {
	informations := []model.DocumentInformation{}
	db := r.AppDB.Model(&model.DocumentInformation{})
	if filter.DocumentID > 0 {
		db.Where("document_id = ?", filter.DocumentID)
	}
	err := db.Find(&informations).Error
	if err != nil {
		return []model.DocumentInformation{}, err
	}
	return informations, nil
}

func (r *documentRepository) UpdateDocumentInformation(document model.DocumentApprove, information model.DocumentInformation) error {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Save(&document).Error
		if err != nil {
			return err
		}

		err = tx.Create(&information).Error
		if err != nil {
			return err
		}

		if information.InformationType == "A" {
			err = tx.Create(&model.ProcessCommand{
				RefID:         strconv.Itoa(int(document.ID)),
				CommandType:   command_enums.EMAIL.String(),
				Payload:       "UpdateDocumentInformation",
				CommandAction: command_action_enums.INFORMATION.String(),
			}).Error
			if err != nil {
				return err
			}
		}

		return nil
	})

	return err
}
