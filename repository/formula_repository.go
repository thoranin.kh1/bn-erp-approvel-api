package repository

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"fmt"

	"gorm.io/gorm"
)

type formulaRepository struct {
	FormulaDB *gorm.DB
}

func NewFormulaRepository(formulaDB *gorm.DB) domain.FormulaRepositoryInterface {
	return &formulaRepository{
		FormulaDB: formulaDB,
	}
}

func (r *formulaRepository) GetCompany(filter model.CompanyFilterModel) ([]model.CompanyModel, error) {
	companys := []model.CompanyModel{}
	db := r.FormulaDB.Model(&model.CompanyModel{})
	if filter.ID != "" {
		db.Where("FCSKID=?", filter.ID)
	}
	err := db.Order("FCCODE ASC").Find(&companys).Error
	if err != nil {
		return companys, fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
	}
	return companys, nil
}

func (r *formulaRepository) GetBranch(filter model.BranchFilterModel) ([]model.BranchModel, error) {
	branchs := []model.BranchModel{}
	db := r.FormulaDB.Model(&model.BranchModel{})
	if filter.ID != "" {
		db.Where("FCSKID=?", filter.ID)
	}
	if filter.CorpID != "" {
		db.Where("FCCORP=?", filter.CorpID)
	}
	err := db.Order("FCCODE ASC").Find(&branchs).Error
	if err != nil {
		return branchs, fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
	}
	return branchs, nil
}

func (r *formulaRepository) Getbook(filter model.BookFilterModel) ([]model.BookModel, error) {
	books := []model.BookModel{}
	db := r.FormulaDB.Model(&model.BookModel{})
	if filter.ID != "" {
		db.Where("FCSKID=?", filter.ID)
	}
	if filter.CorpID != "" {
		db.Where("FCCORP=?", filter.CorpID)
	}
	if filter.BranchID != "" {
		db.Where("FCBRANCH=?", filter.BranchID)
	}
	if filter.Type != "" {
		db.Where("FCREFTYPE=?", filter.Type)
	}
	err := db.Order("FCCODE ASC").Find(&books).Error
	if err != nil {
		return books, fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
	}
	return books, nil
}
