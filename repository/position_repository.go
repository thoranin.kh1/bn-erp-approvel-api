package repository

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
	"context"
	"errors"

	"gorm.io/gorm"
)

type positionRepository struct {
	AppDB *gorm.DB
}

func NewPositionRepository(appDB *gorm.DB) domain.PositionRepositoryInterface {
	return &positionRepository{
		AppDB: appDB,
	}
}

func (r *positionRepository) Get(filter model.PositionFilter) ([]model.Position, error) {
	positions := []model.Position{}
	db := r.AppDB.Model(&model.Position{})
	if filter.ID > 0 {
		db.Where("id = ?", filter.ID)
	}
	err := db.Find(&positions).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return []model.Position{}, nil
	}
	return positions, err
}

func (r *positionRepository) Create(position model.Position, approval []int) (model.Position, error) {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Create(&position).Error
		if err != nil {
			return err
		}

		for _, v := range approval {
			err = tx.Model(&model.Approval{}).Where("id = ?", v).Update("position_id", position.ID).Error
			if err != nil {
				return err
			}
			err = tx.Model(&model.Approval{}).Where("id = ?", v).Update("position_name", position.Name).Error
			if err != nil {
				return err
			}
		}

		return nil
	})
	if err != nil {
		return model.Position{}, err
	}

	data := []model.Approval{}
	err = r.AppDB.Where("position_id = ?", position.ID).Find(&data).Error
	if err != nil {
		return model.Position{}, err
	}

	position.Total = len(data)
	err = r.AppDB.Save(&position).Error
	if err != nil {
		return model.Position{}, err
	}

	return position, nil
}

func (r *positionRepository) Update(position model.Position, approval []int) (model.Position, error) {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Save(&position).Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.Approval{}).Where("position_id = ?", position.ID).Update("position_name", "").Error
		if err != nil {
			return err
		}

		err = tx.Model(&model.Approval{}).Where("position_id = ?", position.ID).Update("position_id", 0).Error
		if err != nil {
			return err
		}

		for _, v := range approval {
			err = tx.Model(&model.Approval{}).Where("id = ?", v).Update("position_id", position.ID).Error
			if err != nil {
				return err
			}
			err = tx.Model(&model.Approval{}).Where("id = ?", v).Update("position_name", position.Name).Error
			if err != nil {
				return err
			}
		}

		err = tx.Where("approval_position_id01=?", position.ID).Updates(model.Setting{ApprovalPositionName01: position.Name}).Error
		if err != nil {
			return err
		}

		err = tx.Where("approval_position_id02=?", position.ID).Updates(model.Setting{ApprovalPositionName02: position.Name}).Error
		if err != nil {
			return err
		}

		err = tx.Where("approval_position_id03=?", position.ID).Updates(model.Setting{ApprovalPositionName03: position.Name}).Error
		if err != nil {
			return err
		}

		err = tx.Where("approval_position_id04=?", position.ID).Updates(model.Setting{ApprovalPositionName04: position.Name}).Error
		if err != nil {
			return err
		}

		err = tx.Where("approval_position_id05=?", position.ID).Updates(model.Setting{ApprovalPositionName05: position.Name}).Error
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return model.Position{}, err
	}

	data := []model.Approval{}
	err = r.AppDB.Where("position_id = ?", position.ID).Find(&data).Error
	if err != nil {
		return model.Position{}, err
	}

	position.Total = len(data)
	err = r.AppDB.Save(&position).Error
	if err != nil {
		return model.Position{}, err
	}

	return position, nil
}

func (r *positionRepository) Delete(id int) error {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Where("id=?", id).Delete(&model.Position{}).Error
		if err != nil {
			return err
		}

		err = tx.Where("position_id=?", id).Updates(model.Approval{PositionID: 0, PositionName: ""}).Error
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return err
	}
	return nil
}
