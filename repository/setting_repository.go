package repository

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/command_action_enums"
	"bn-erp-approvel-api/enum/command_enums"
	"bn-erp-approvel-api/model"
	"context"
	"errors"
	"strconv"

	"gorm.io/gorm"
)

type settingRepository struct {
	DocumentType      []string
	DocumentTypeAllow []string
	AppDB             *gorm.DB
}

func NewSettingRepository(documentTypeAllow []string, appDB *gorm.DB) domain.SettingRepositoryInterface {
	return &settingRepository{
		DocumentType:      []string{"PO", "PR", "PB"},
		DocumentTypeAllow: documentTypeAllow,
		AppDB:             appDB,
	}
}

func (r *settingRepository) GetDocumentType() ([]model.DocumentType, error) {
	documentTypes := []model.DocumentType{}

	documentAllow := r.DocumentTypeAllow
	if len(documentAllow) > 0 {
		if documentAllow[0] == "*" {
			documentAllow = r.DocumentType
		}
	}
	for _, v := range documentAllow {
		name := ""
		switch v {
		case "PO":
			name = v
		case "PR":
			name = v
		case "PB":
			name = v
		default:
			continue
		}
		documentTypes = append(documentTypes, model.DocumentType{
			Code: v,
			Name: name,
		})
	}
	return documentTypes, nil
}

func (r *settingRepository) GetSetting(filter model.SettingFilter) ([]model.Setting, error) {
	settings := []model.Setting{}
	db := r.AppDB.Model(model.Setting{})
	if filter.ID > 0 {
		db.Where("id = ?", filter.ID)
	}

	if filter.TypeCode != "" {
		db.Where("type_code = ?", filter.TypeCode)
	}

	if filter.BookID != "" {
		db.Where("book_id = ?", filter.BookID)
	}

	if filter.BookCode != "" {
		db.Where("book_code = ?", filter.BookCode)
	}

	if filter.CorpID != "" {
		db.Where("corp_id = ?", filter.CorpID)
	}

	if filter.BranchID != "" {
		db.Where("branch_id = ?", filter.BranchID)
	}

	err := db.Find(&settings).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return []model.Setting{}, nil
	}
	return settings, err
}

func (r *settingRepository) CreateSetting(setting model.Setting) (model.Setting, error) {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Create(&setting).Error
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return model.Setting{}, err
	}

	return setting, nil
}

func (r *settingRepository) UpdateSetting(setting model.Setting) (model.Setting, error) {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Save(&setting).Error
		if err != nil {
			return err
		}

		err = tx.Create(&model.ProcessCommand{
			RefID:         strconv.Itoa(int(setting.ID)),
			CommandType:   command_enums.SETTING.String(),
			CommandAction: command_action_enums.UPDATE.String(),
		}).Error

		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return model.Setting{}, err
	}
	return setting, err
}

func (r *settingRepository) DeleteSetting(id uint) error {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Where("id=?", id).Delete(&model.Setting{}).Error
		if err != nil {
			return err
		}

		err = tx.Create(&model.ProcessCommand{
			RefID:         strconv.Itoa(int(id)),
			CommandType:   command_enums.SETTING.String(),
			CommandAction: command_action_enums.DELETE.String(),
		}).Error
		if err != nil {
			return err
		}
		return nil
	})
	return err
}
