package repository

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"context"
	"errors"
	"fmt"

	"gorm.io/gorm"
)

type userRepository struct {
	AppDB *gorm.DB
}

func NewUserRepository(appDB *gorm.DB) domain.UserRepositoryInterface {
	return &userRepository{
		AppDB: appDB,
	}
}

func (r *userRepository) GetUser(filter model.UserFilter) ([]model.User, error) {
	users := []model.User{}
	db := r.AppDB.Model(&model.User{})
	if filter.ID > 0 {
		db.Where("id = ?", filter.ID)
	}
	if filter.Username != "" {
		db.Where("username = ?", filter.Username)
	}
	err := db.Find(&users).Error
	if errors.Is(err, gorm.ErrRecordNotFound) {
		return []model.User{}, nil
	}
	return users, err
}

func (r *userRepository) CreateUser(user model.User, permissions []model.Permission) (model.User, error) {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Create(&user).Error
		if err != nil {
			return err
		}

		for i := range permissions {
			permissions[i].UserID = user.ID
		}

		err = tx.Create(&permissions).Error
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

func (r *userRepository) UpdateUser(user model.User, permissions []model.Permission) (model.User, error) {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		err := tx.Save(&user).Error
		if err != nil {
			return err
		}

		err = tx.Where("user_id=?", user.ID).Delete(&model.Permission{}).Error
		if err != nil {
			return err
		}

		err = tx.Create(&permissions).Error
		if err != nil {
			return err
		}

		return nil
	})
	if err != nil {
		return model.User{}, err
	}
	return user, nil
}

func (r *userRepository) UpdateUserProfile(user model.User) error {
	err := r.AppDB.Save(&user).Error
	if err != nil {
		return err
	}
	return nil
}

func (r *userRepository) DeleteUser(id uint) error {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {

		err := tx.Where("id=?", id).Delete(&model.User{}).Error
		if err != nil {
			return err
		}

		err = tx.Where("user_id=?", id).Delete(&model.Permission{}).Error
		if err != nil {
			return err
		}

		return nil
	})
	return err
}

func (r *userRepository) GetToken(key string) (model.Token, error) {
	token := model.Token{}
	err := r.AppDB.Where("token=?", key).First(&token).Error
	if err != nil {
		return token, fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
	}
	return token, nil
}

func (r *userRepository) CreateToken(token model.Token) (model.Token, error) {
	result := r.AppDB.Create(&token)
	if result.Error != nil {
		return token, fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), result.Error)
	}
	return token, nil
}

func (r *userRepository) DeleteToken(key string) error {
	err := r.AppDB.Where("token=?", key).Delete(&model.Token{}).Error
	if err != nil {
		return fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
	}
	return nil
}

func (r *userRepository) CreateOrUpdateMenu(menus []string) error {
	err := r.AppDB.WithContext(context.Background()).Transaction(func(tx *gorm.DB) error {
		for _, v := range menus {
			result := tx.Model(&model.Menu{}).Where("code = ?", v).Update("name", v)
			if result.Error != nil {
				return result.Error
			}
			if result.RowsAffected == 0 {
				err := tx.Create(&model.Menu{Code: v, Name: v}).Error
				if err != nil {
					return err
				}
			}
		}
		return nil
	})

	if err != nil {
		return err
	}

	return nil
}

func (r *userRepository) GetMenu() ([]model.Menu, error) {
	menus := []model.Menu{}
	err := r.AppDB.Find(&menus).Error
	if err != nil {
		return menus, fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
	}
	return menus, nil
}

func (r *userRepository) GetUserPermission(userID int) ([]model.Permission, error) {
	permission := []model.Permission{}
	err := r.AppDB.Where("user_id", userID).Find(&permission).Error
	return permission, err
}
