package main

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/library"
	"bn-erp-approvel-api/model"
	"bn-erp-approvel-api/repository"
	"bn-erp-approvel-api/usecase"
	"bn-erp-approvel-api/util"
	"fmt"
	"os"
	"strings"
	"time"

	"github.com/shirou/gopsutil/host"
	"github.com/spf13/viper"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

func readConfig(path string) {
	configPath := path + "/config.yaml"
	fmt.Println(configPath)

	viper.SetConfigFile(configPath)
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		for {
			time.Sleep(25 * time.Hour)
		}
	}
}

func GetServerKey() (string, error) {
	host, err := host.Info()
	if err != nil {
		return "", err
	}

	hostname, err := os.Hostname()
	if err != nil {
		return "", err
	}
	serverKey := hostname + host.HostID
	serverKey = strings.ReplaceAll(serverKey, "-", "")
	return serverKey, nil
}

func GetServerKeyHash() (string, error) {
	serverKey, err := GetServerKey()
	if err != nil {
		return "", err
	}
	c, err := util.EncryptServer(serverKey)
	if err != nil {
		return "", err
	}
	return c, nil
}

func main() {
	path := "C:/erp_approve"
	// path = "/Users/thoraninkhumpiphop/go/src/bn-erp-approvel-api"
	readConfig(path)

	dsn := viper.GetString("database.applicarion")
	dsnFormula := viper.GetString("database.formula")
	approveID := viper.GetString("app.approve_id")

	serverKey, err := GetServerKey()
	if err != nil {
		fmt.Println("Server Key : ", err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	license := viper.GetString("app.license")
	license = util.B64Decode(license)
	licenseData, err := util.Decrypt(serverKey, license)

	if err != nil {
		fmt.Println("Server Key : ", err)

		serverKeyHash, err := GetServerKeyHash()
		if err != nil {
			fmt.Println("Server Key : ", err)
			for {
				time.Sleep(12 * time.Hour)
			}
		}
		file, err := os.Create(path + "/erp_approval_server_key.txt") //create a new file
		if err != nil {
			fmt.Println("Create", err)
			for {
				time.Sleep(12 * time.Hour)
			}
		}
		file.WriteString(util.B64Encode(serverKeyHash))
		defer file.Close()

		for {
			time.Sleep(12 * time.Hour)
		}
	}
	licenseData = strings.ReplaceAll(licenseData, "document_types:", "")
	document_types := strings.Split(licenseData, ",")

	environment := library.NewEnvironment()
	token := library.NewTokenLibrary("TOKEN")

	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{
		NowFunc: environment.Now,
	})
	if err != nil {
		fmt.Println(err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	dbFormula, err := gorm.Open(sqlserver.Open(dsnFormula), &gorm.Config{
		NowFunc: environment.Now,
	})
	if err != nil {
		fmt.Println(err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	db.AutoMigrate(&model.DocumentApprove{})
	db.AutoMigrate(&model.ProcessCommand{})

	approvalRepository := repository.NewApproveRepository(db)
	positionRepository := repository.NewPositionRepository(db)
	userRepository := repository.NewUserRepository(db)
	formulaRepository := repository.NewFormulaRepository(dbFormula)
	settingRepository := repository.NewSettingRepository(document_types, db)
	documentRepository := repository.NewDocumentRepository(db, dbFormula, approveID)

	approvalUsecase := usecase.NewApproveUsecase(approvalRepository)
	positionUsecase := usecase.NewpositonUsecase(positionRepository)
	userUsecase := usecase.NewUserUsecase(userRepository, token, "rootPassword")
	formulaUsecase := usecase.NewFormulaUsecase(formulaRepository)
	settingUsecase := usecase.NewSettingUsecase(settingRepository, positionUsecase, formulaUsecase)
	documentUsecase := usecase.NewDocumentUsecase(documentRepository, settingUsecase, approvalUsecase, userUsecase, environment)

	for {
		process(db, documentUsecase)
		time.Sleep(1 * time.Second)
	}

}

func process(appDB *gorm.DB, documentUsecase domain.DocumentUsecaseInterface) {
	command := model.ProcessCommand{}
	db := appDB.Model(&model.ProcessCommand{})
	db.Where("command_type = 'DOCUMENT'")
	db.Where("message IS NULL or message = ''")
	db.Where("updated_at <= GETDATE()")

	err := db.First(&command).Error
	if err != nil {
		fmt.Println(err)
	}

	if command.ID <= 0 {
		return
	}

	message := ""
	switch command.CommandAction {
	case "INSERT":
		err := documentUsecase.Create(int(command.ID))
		if err != nil {
			message = err.Error()
		} else {
			message = "SUCCESS."
		}
	case "UPDATE":
		err := documentUsecase.ResetDocument(int(command.ID))
		if err != nil {
			message = err.Error()
		} else {
			message = "SUCCESS."
		}
	default:
		message = "NOT."
	}

	command.Message = message
	err = appDB.Save(&command).Error
	if err != nil {
		fmt.Println(err)
	}
}
