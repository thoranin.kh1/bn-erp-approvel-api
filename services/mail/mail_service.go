package main

import (
	"bn-erp-approvel-api/enum/command_action_enums"
	"bn-erp-approvel-api/enum/document_type_enums"
	"bn-erp-approvel-api/library"
	"bn-erp-approvel-api/model"
	"bytes"
	"database/sql"
	"errors"
	"fmt"
	"html/template"
	"strconv"
	"strings"
	"time"

	"github.com/leekchan/accounting"
	"github.com/spf13/viper"
	"gopkg.in/gomail.v2"
	"gorm.io/driver/sqlserver"
	"gorm.io/gorm"
)

func readConfig(path string) {
	configPath := path + "/config.yaml"
	fmt.Println(configPath)

	viper.SetConfigFile(configPath)
	if err := viper.ReadInConfig(); err != nil {
		fmt.Println(err)
		for {
			time.Sleep(25 * time.Hour)
		}
	}
}

func main() {
	path := "C:/erp_approve"
	// path = "/Users/thoraninkhumpiphop/go/src/bn-erp-approvel-api"
	readConfig(path)

	dsn := viper.GetString("database.applicarion")
	dsnFormula := viper.GetString("database.formula")
	apiUri := viper.GetString("app.api_uri")
	fnUri := viper.GetString("app.fn_uri")

	environment := library.NewEnvironment()

	db, err := gorm.Open(sqlserver.Open(dsn), &gorm.Config{
		NowFunc: environment.Now,
	})
	if err != nil {
		fmt.Println(err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	dbFormula, err := gorm.Open(sqlserver.Open(dsnFormula), &gorm.Config{
		NowFunc: environment.Now,
	})
	if err != nil {
		fmt.Println(err)
		for {
			time.Sleep(12 * time.Hour)
		}
	}

	db.AutoMigrate(&model.ProcessCommand{})

	for {
		process(db, dbFormula, apiUri, fnUri, path)
		time.Sleep(1 * time.Second)
	}
}

func process(appDB *gorm.DB, dbFormula *gorm.DB, apiUri string, fnUri string, path string) {
	command := model.ProcessCommand{}
	db := appDB.Model(&model.ProcessCommand{})
	db.Where("command_type = 'EMAIL'")
	db.Where("message IS NULL or message = ''")
	err := db.First(&command).Error
	if err != nil {
		fmt.Println(err)
	}

	if command.ID <= 0 {
		return
	}

	refID, _ := strconv.Atoi(command.RefID)
	if refID <= 0 {
		return
	}

	message := "SUCCESS."
	err = start(appDB, dbFormula, refID, apiUri, fnUri, path, command.CommandAction)
	if err != nil {
		message = err.Error()
	}

	command.Message = message
	err = appDB.Save(&command).Error
	if err != nil {
		fmt.Println(err)
	}
}

func start(appDB *gorm.DB, dbFormula *gorm.DB, id int, apiUri string, fnUri string, path string, commandType string) error {
	document := model.DocumentApprove{}
	err := appDB.First(&document, id).Error
	if err != nil {
		return fmt.Errorf("%s %w", "DocumentApprove", err)
	}

	approves := []model.DocumentApproveList{}
	err = appDB.Model(&model.DocumentApproveList{}).Where("document_id = ?", document.ID).Where("approval_level = ?", document.StepCondition).Find(&approves).Error
	if err != nil {
		return fmt.Errorf("%s %w", "DocumentApproveList", err)
	}

	information := []model.DocumentInformation{}
	appDB.Model(&model.DocumentInformation{}).Where("document_id = ?", document.ID).Find(&information)
	fmt.Println(commandType)
	fmt.Println(information)

	switch document.TypeCode {
	case document_type_enums.PO.String():
		err = SentPODATA(dbFormula, document, approves, apiUri, fnUri, path, commandType, information)
	case document_type_enums.PR.String():
		err = SentPRDATA(dbFormula, document, approves, apiUri, fnUri, path, commandType, information)
	case document_type_enums.PB.String():
		err = SentPBDATA(dbFormula, document, approves, apiUri, fnUri, path, commandType, information)
	}
	if err != nil {
		return err
	}

	return nil

}

func SentPRDATA(dbFormula *gorm.DB, document model.DocumentApprove, approves []model.DocumentApproveList, apiUri string, fnUri string, path string, commandType string, information []model.DocumentInformation) error {
	datas, err := GetPOPRDocument(dbFormula, document.DocumentRefID)
	if err != nil {
		return err
	}

	if len(datas) <= 0 {
		return errors.New("GetPOPRDocument NOT FOUND")
	}

	data := datas[0]

	ac := accounting.Accounting{Symbol: "", Precision: 6}
	items := []model.PrItemTemplate{}
	var total float64
	for i, v := range datas {
		total += (v.Price * v.Qty)
		items = append(items, model.PrItemTemplate{
			NO:        strconv.Itoa(i + 1),
			ITEM_CODE: v.ItemCode,
			ITEM_NAME: v.ItemName,
			PRICE:     ac.FormatMoney(v.Price),
			QTY:       ac.FormatMoney(v.Qty),
			AMOUNT:    ac.FormatMoney((v.Price * v.Qty)),
		})
	}

	isInformation := false
	if commandType == command_action_enums.INFORMATION.String() {
		isInformation = true
	}

	informationData := []model.InformationTemplate{}
	for _, v := range information {
		informationData = append(informationData, model.InformationTemplate{
			INFORMATION_TYPE: v.InformationType,
			MESSAGE:          v.Message,
		})
	}

	for _, v := range approves {
		template := model.PrTemplate{
			SENT:             fmt.Sprintf("%s %s", v.ApprovalFirstName, v.ApprovalLastName),
			CORP:             data.CorpName,
			BRANCH:           data.BranchName,
			DOCUMENT_CODE:    data.DocumentCode,
			DOCUMENT_DATE:    convertDateSQL(data.DocumentDate),
			REF_NO:           data.DocumentRefNo,
			BOOK_CODE:        data.BookCode,
			SUPP_CODE:        data.SuppCode,
			SUPP_NAME:        data.SuppName,
			SUB_TOTAL:        ac.FormatMoney(total),
			DIS:              ac.FormatMoney(data.Dis),
			VAT:              ac.FormatMoney(data.Vat),
			TOTAL:            ac.FormatMoney(data.Total),
			APPROVE_LINK:     fmt.Sprintf("%s/document/approve/%s", fnUri, v.TransactionKey),
			REJECRT_LINK:     fmt.Sprintf("%s/document/reject/%s", fnUri, v.TransactionKey),
			INFORMATION_LINK: fmt.Sprintf("%s/document/information/%s", fnUri, v.TransactionKey),
			ITEMS:            items,
			IS_INFORMATION:   isInformation,
			INFORMATION_DATA: informationData,
			CREATE_BY:        data.CreateBy,
			SECTION_NAME:     data.SectionName,
			PROJECT_NAME:     data.ProjectName,
		}

		body, err := MapTemplate(fmt.Sprintf("%s/template/pr.html", path), template)
		if err != nil {
			return err
		}

		m := gomail.NewMessage()
		m.SetHeader("From", viper.GetString("mail.email"))
		m.SetHeader("To", v.ApprovalEmail)
		m.SetHeader("Subject", "ขออนุมัติเอกสารเลขที่ "+data.DocumentCode)
		m.SetBody("text/html", body)

		d := gomail.NewDialer(
			viper.GetString("mail.host"),
			viper.GetInt("mail.port"),
			viper.GetString("mail.email"),
			viper.GetString("mail.pass"),
		)
		if err := d.DialAndSend(m); err != nil {
			return err
		}
	}

	return nil
}

func SentPODATA(dbFormula *gorm.DB, document model.DocumentApprove, approves []model.DocumentApproveList, apiUri string, fnUri string, path string, commandType string, information []model.DocumentInformation) error {
	datas, err := GetPOPRDocument(dbFormula, document.DocumentRefID)
	if err != nil {
		return err
	}

	if len(datas) <= 0 {
		return errors.New("GetPOPRDocument NOT FOUND")
	}

	data := datas[0]

	items := []model.PrItemTemplate{}
	var total float64
	ac := accounting.Accounting{Symbol: "", Precision: 6}
	for i, v := range datas {
		total += (v.Price * v.Qty)
		items = append(items, model.PrItemTemplate{
			NO:        strconv.Itoa(i + 1),
			ITEM_CODE: v.ItemCode,
			ITEM_NAME: v.ItemName,
			PRICE:     ac.FormatMoney(v.Price),
			QTY:       ac.FormatMoney(v.Qty),
			AMOUNT:    ac.FormatMoney((v.Price * v.Qty)),
		})
	}

	isInformation := false
	if commandType == command_action_enums.INFORMATION.String() {
		isInformation = true
	}

	informationData := []model.InformationTemplate{}
	for _, v := range information {
		informationData = append(informationData, model.InformationTemplate{
			INFORMATION_TYPE: v.InformationType,
			MESSAGE:          v.Message,
		})
	}

	for _, v := range approves {
		template := model.PrTemplate{
			SENT:             fmt.Sprintf("%s %s", v.ApprovalFirstName, v.ApprovalLastName),
			CORP:             data.CorpName,
			BRANCH:           data.BranchName,
			DOCUMENT_CODE:    data.DocumentCode,
			DOCUMENT_DATE:    convertDateSQL(data.DocumentDate),
			REF_NO:           data.DocumentRefNo,
			BOOK_CODE:        data.BookCode,
			SUPP_CODE:        data.SuppCode,
			SUPP_NAME:        data.SuppName,
			SUB_TOTAL:        ac.FormatMoney(total),
			DIS:              ac.FormatMoney(data.Dis),
			VAT:              ac.FormatMoney(data.Vat),
			TOTAL:            ac.FormatMoney(data.Total),
			APPROVE_LINK:     fmt.Sprintf("%s/document/approve/%s", fnUri, v.TransactionKey),
			REJECRT_LINK:     fmt.Sprintf("%s/document/reject/%s", fnUri, v.TransactionKey),
			INFORMATION_LINK: fmt.Sprintf("%s/document/information/%s", fnUri, v.TransactionKey),
			ITEMS:            items,
			IS_INFORMATION:   isInformation,
			INFORMATION_DATA: informationData,
			CREATE_BY:        data.CreateBy,
			SECTION_NAME:     data.SectionName,
			PROJECT_NAME:     data.ProjectName,
		}

		body, err := MapTemplate(fmt.Sprintf("%s/template/po.html", path), template)
		if err != nil {
			return err
		}

		m := gomail.NewMessage()
		m.SetHeader("From", viper.GetString("mail.email"))
		m.SetHeader("To", v.ApprovalEmail)
		m.SetHeader("Subject", "ขออนุมัติเอกสารเลขที่ "+data.DocumentCode)
		m.SetBody("text/html", body)

		d := gomail.NewDialer(
			viper.GetString("mail.host"),
			viper.GetInt("mail.port"),
			viper.GetString("mail.email"),
			viper.GetString("mail.pass"),
		)
		if err := d.DialAndSend(m); err != nil {
			return err
		}
	}
	return nil
}

func SentPBDATA(dbFormula *gorm.DB, document model.DocumentApprove, approves []model.DocumentApproveList, apiUri string, fnUri string, path string, commandType string, information []model.DocumentInformation) error {
	datas, err := GetPBDocument(dbFormula, document.DocumentRefID)
	if err != nil {
		return err
	}

	if len(datas) <= 0 {
		return errors.New("GetPBDocument NOT FOUND")
	}

	data := datas[0]

	ac := accounting.Accounting{Symbol: "", Precision: 6}
	items := []model.PbItemTemplate{}
	for i, v := range datas {
		items = append(items, model.PbItemTemplate{
			NO:            strconv.Itoa(i + 1),
			FCREFTYPE:     v.RefType,
			BOOK_CODE:     v.BookCode,
			GLREF_CODE:    v.GlRefCode,
			GLREF_REFNO:   v.GlRefRefNo,
			GLREF_DUEDATE: convertDateSQL(v.GlRefDueDate),
			AMT:           ac.FormatMoney(v.Amount),
			PAYAMT:        ac.FormatMoney(v.PayAmount),
			FNVATAMTKE:    ac.FormatMoney(v.XRate),
			FNAMT:         ac.FormatMoney(v.VatAmount),
		})
	}

	isInformation := false
	if commandType == command_action_enums.INFORMATION.String() {
		isInformation = true
	}

	informationData := []model.InformationTemplate{}
	for _, v := range information {
		informationData = append(informationData, model.InformationTemplate{
			INFORMATION_TYPE: v.InformationType,
			MESSAGE:          v.Message,
		})
	}

	template := model.PbTemplate{
		BOOK_CODE:        data.BookCode,
		DOCUMENT_CODE:    data.DocumentCode,
		REF_NO:           data.DocumentRefNo,
		DOCUMENT_DATE:    convertDateSQL(data.DocumentDate),
		SUPP_CODE:        data.SuppCode,
		SUPP_NAME:        data.SuppName,
		PAY_DATE:         convertDateSQL(data.PayDate),
		PAY_CODE:         data.PayCode,
		PAY_NAME:         data.PayName,
		FCPAYRECV2:       data.PayRecV2,
		BANK_CODE:        data.BankCode,
		BANK_NAME:        data.BankName,
		CHQ_AMT:          ac.FormatMoney(data.ChqAmt),
		FNAMT:            ac.FormatMoney(data.FnAmt),
		FNVATAMT:         ac.FormatMoney(data.FnVatAmt),
		TOTAL:            ac.FormatMoney(data.Total),
		IS_INFORMATION:   isInformation,
		INFORMATION_DATA: informationData,
		ITEMS:            items,
		CREATE_BY:        data.CreateBy,
		SECTION_NAME:     data.SectionName,
		PROJECT_NAME:     data.ProjectName,
	}

	for _, v := range approves {
		template.SENT = fmt.Sprintf("%s %s", v.ApprovalFirstName, v.ApprovalLastName)
		template.APPROVE_LINK = fmt.Sprintf("%s/document/approve/%s", fnUri, v.TransactionKey)
		template.REJECRT_LINK = fmt.Sprintf("%s/document/reject/%s", fnUri, v.TransactionKey)
		template.INFORMATION_LINK = fmt.Sprintf("%s/document/information/%s", fnUri, v.TransactionKey)

		body, err := MapTemplate(fmt.Sprintf("%s/template/pb.html", path), template)
		if err != nil {
			return err
		}

		m := gomail.NewMessage()
		m.SetHeader("From", viper.GetString("mail.email"))
		m.SetHeader("To", v.ApprovalEmail)
		m.SetHeader("Subject", "ขออนุมัติเอกสารเลขที่ "+data.DocumentCode)
		m.SetBody("text/html", body)

		d := gomail.NewDialer(
			viper.GetString("mail.host"),
			viper.GetInt("mail.port"),
			viper.GetString("mail.email"),
			viper.GetString("mail.pass"),
		)
		if err := d.DialAndSend(m); err != nil {
			return err
		}
	}

	return nil
}

func GetPOPRDocument(dbFormula *gorm.DB, id string) ([]model.DataDocument, error) {
	query := `SELECT 
	ORDERH.FCCODE AS DOC_CODE,
	FCREFNO,
	ORDERH.FDDATE,
	PROD.FCCODE AS PD_CODE,
	PROD.FCNAME AS PD_NAME,
	ORDERI.FNPRICE,
	ORDERI.FNQTY,
	UM.FCCODE AS UM_CODE,
	UM.FCNAME AS UM_NAME,
	COOR.FCCODE AS SUB_CODE,
	COOR.FCNAME AS SUB_NAME,
	ORDERH.FTDATETIME,
	ORDERH.FTLASTEDIT,
	ORDERH.FCAPPROVEB,
	ORDERH.FDAPPROVE,
	BOOK.FCCODE AS BOOK_CODE,
	BOOK.FCNAME AS BOOK_NAME,
	CORP.FCNAME AS CORP_NAME,
	BRANCH.FCNAME AS BRANCH_NAME,
	ORDERH.FCU3STATUS,
	RTRIM(LTRIM(EMPLR.FCLOGIN)) AS CREATEBY, --ADD 21/01/2024
	RTRIM(LTRIM(SECT.FCNAME)) AS SECT_NAME, --ADD 21/01/2024
	RTRIM(LTRIM(PROJ.FCNAME)) AS PROJ_NAME,  --ADD 21/01/2024
	ORDERH.FNDISCAMTK AS DIS, --ADD 21/01/2024
	ORDERH.FNVATAMT AS VAT, 
	ORDERH.FNAMT + ORDERH.FNVATAMT AS TOTAL
	from ORDERH 
	LEFT JOIN ORDERI ON  ORDERH.FCSKID = ORDERI.FCORDERH 
	LEFT JOIN PROD ON  PROD.FCSKID = ORDERI.FCPROD 
	LEFT JOIN UM ON  UM.FCSKID = ORDERI.FCUM 
	LEFT JOIN COOR ON COOR.FCSKID = ORDERH.FCCOOR
	LEFT JOIN BOOK ON ORDERH.FCBOOK = BOOK.FCSKID
	LEFT JOIN CORP ON ORDERH.FCCORP = CORP.FCSKID
	LEFT JOIN BRANCH ON ORDERH.FCBRANCH = CORP.FCSKID
	LEFT JOIN EMPLR ON ORDERH.FCCREATEBY = EMPLR.FCSKID  --ADD 21/01/2024
	LEFT JOIN SECT ON ORDERH.FCSECT = SECT.FCSKID  --ADD 21/01/2024
	LEFT JOIN PROJ ON ORDERH.FCPROJ = PROJ.FCSKID  --ADD 21/01/2024
	WHERE ORDERH.FCSKID = ?
	AND ORDERH.FCU3STATUS='1' 
	AND ORDERH.FCSTAT <> 'C'`

	documents := []model.DataDocument{}
	params := []interface{}{id}
	err := dbFormula.Raw(query, params...).Scan(&documents).Error
	if err != nil {
		return []model.DataDocument{}, err
	}
	return documents, nil
}

func GetPBDocument(dbFormula *gorm.DB, id string) ([]model.PBDataDocument, error) {
	query := `SELECT 
	RTRIM(LTRIM(PRCVH.FCCODE)) AS DOC_CODE,  
	RTRIM(LTRIM(PRCVH.FCREFNO)) AS REF_NO,
	PRCVH.FDDATE AS DOC_DATE,
	RTRIM(LTRIM(COOR.FCCODE)) AS SUPP_CODE,
	RTRIM(LTRIM(COOR.FCNAME)) AS SUPP_NAME,
	GLREF.FCREFTYPE,
	BOOK.FCCODE AS BOOK_CODE, 
	RTRIM(LTRIM(GLREF.FCCODE)) AS GLREF_CODE, 
	RTRIM(LTRIM(GLREF.FCREFNO)) AS GLREF_REFNO,
	GLREF.FDDUEDATE AS GLREF_DUEDATE,
	GLREF.FNAMT+ GLREF.FNVATAMT AS AMT,
	PRCVI.FNPAYAMT AS PAYAMT,
	VATTYPE.FNRATE AS FNXRATE,
	PRCVI.FNVATAMTKE,
	PAYMENT.FDDUEDATE AS PAY_DATE,
	RTRIM(LTRIM(PAYMENT.FCPAYTYPE)) AS PAY_CODE,
	RTRIM(LTRIM(PAYTYPE.FCNAME)) AS PAY_NAME,
	PAYMENT.FCPAYRECV AS FCPAYRECV2,
	RTRIM(LTRIM(BANK.FCCODE)) AS BANK_CODE,
	RTRIM(LTRIM(BANK.FCNAME)) AS BANK_NAME,
	RTRIM(LTRIM(PAYMENT.FCBBRANCH)) AS BBRANCH,
	PRCVH.FNVATAMT,
	PRCVH.FNAMT,
	PRCVH.FNVATAMT+PRCVH.FNAMT AS TOTAL,
	PAYMENT.FNAMT AS CHQ_AMT,
	RTRIM(LTRIM(EMPLR.FCLOGIN)) AS CREATEBY, --ADD 21/01/2024
	RTRIM(LTRIM(SECT.FCNAME)) AS SECT_NAME, --ADD 21/01/2024
	RTRIM(LTRIM(PROJ.FCNAME)) AS PROJ_NAME  --ADD 21/01/2024
	from PRCVH LEFT JOIN PRCVI ON PRCVH.FCSKID = PRCVI.FCPRCVH
	LEFT JOIN GLREF ON  GLREF.FCSKID = PRCVI.FCCHILDGLR
	LEFT JOIN BOOK ON GLREF.FCBOOK = BOOK.FCSKID
	LEFT JOIN COOR ON PRCVH.FCCOOR = COOR.FCSKID
	LEFT JOIN BILPAY ON PRCVH.FCSKID = BILPAY.FCMASTERH
	LEFT JOIN PAYMENT ON BILPAY.FCPAYMENT = PAYMENT.FCSKID
	LEFT JOIN BANK ON PAYMENT.FCBANK = BANK.FCSKID
	LEFT JOIN PAYTYPE ON PAYMENT.FCPAYTYPE = PAYTYPE.FCCODE
	LEFT JOIN EMPLR ON PRCVH.FCCREATEBY = EMPLR.FCSKID  --ADD 21/01/2024
	LEFT JOIN SECT ON PRCVH.FCSECT = SECT.FCSKID  --ADD 21/01/2024
	LEFT JOIN PROJ ON PRCVH.FCPROJ = PROJ.FCSKID  --ADD 21/01/2024
	LEFT JOIN VATTYPE ON PRCVH.FCVATTYPE = VATTYPE.FCCODE
	WHERE PRCVH.FCREFTYPE='PB' and PRCVH.FCSTAT <> 'C' and PRCVH.FCSKID= ?`
	documents := []model.PBDataDocument{}
	params := []interface{}{id}
	err := dbFormula.Raw(query, params...).Scan(&documents).Error
	if err != nil {
		return []model.PBDataDocument{}, err
	}
	return documents, nil
}

func MapTemplate(templateFileName string, data interface{}) (string, error) {
	t, err := template.ParseFiles(templateFileName)
	if err != nil {
		return "", err
	}
	buf := new(bytes.Buffer)
	if err = t.Execute(buf, data); err != nil {
		return "", err
	}
	return buf.String(), nil
}

func convertDateSQL(date sql.NullTime) string {
	if !date.Valid {
		return ""
	}
	dateString := date.Time.Format("2006-01-02")
	dates := strings.Split(dateString, "-")
	year, err := strconv.Atoi(dates[0])
	if err != nil {
		return ""
	}
	return fmt.Sprintf("%s/%s/%d", dates[2], dates[1], (year + 543))
}
