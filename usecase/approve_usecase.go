package usecase

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
)

type approveUsecase struct {
	ApproveRepository domain.ApproveRepositoryInterface
}

func NewApproveUsecase(
	approveRepository domain.ApproveRepositoryInterface,
) domain.ApproveUsecaseInterface {
	return &approveUsecase{
		ApproveRepository: approveRepository,
	}
}

func (u *approveUsecase) GetApproval(filter model.ApprovalFilter) ([]model.Approval, error) {
	approvals, err := u.ApproveRepository.Get(filter)
	if err != nil {
		return []model.Approval{}, err
	}
	return approvals, nil
}

func (u *approveUsecase) GetApprovalByID(id int) (model.Approval, error) {
	if id <= 0 {
		return model.Approval{}, nil
	}
	approvals, err := u.ApproveRepository.Get(model.ApprovalFilter{
		ID: id,
	})
	if err != nil {
		return model.Approval{}, err
	}
	return approvals[0], err
}

func (u *approveUsecase) Create(req model.ApprovalAction) (model.Approval, error) {
	approval := model.Approval{
		FirstName: req.FirstName,
		LastName:  req.LastName,
		Email:     req.Email,
		Remark:    req.Remark,
	}

	data, err := u.ApproveRepository.Create(approval)
	if err != nil {
		return model.Approval{}, nil
	}

	return data, nil
}

func (u *approveUsecase) Update(req model.ApprovalAction) (model.Approval, error) {
	approval, err := u.GetApprovalByID(req.ID)
	if err != nil {
		return model.Approval{}, nil
	}

	approval.FirstName = req.FirstName
	approval.LastName = req.LastName
	approval.Email = req.Email
	approval.Remark = req.Remark
	data, err := u.ApproveRepository.Update(approval)
	if err != nil {
		return model.Approval{}, nil
	}

	return data, nil
}

func (u *approveUsecase) Delete(id int) error {
	err := u.ApproveRepository.Delete(id)
	return err
}
