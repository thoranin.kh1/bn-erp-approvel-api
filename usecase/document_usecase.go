package usecase

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/document_status_enums"
	"bn-erp-approvel-api/enum/document_type_enums"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"errors"
	"strings"

	"github.com/google/uuid"
)

type documentUsecase struct {
	DocumentRepository domain.DocumentRepositoryInterface
	SettingUsecase     domain.SettingUsecaseInterface
	ApproveUsecase     domain.ApproveUsecaseInterface
	UserUsecase        domain.UserUsecaseInterface
	Environment        domain.EnvironmentLibraryInterface
}

func NewDocumentUsecase(
	documentRepository domain.DocumentRepositoryInterface,
	settingUsecase domain.SettingUsecaseInterface,
	approveUsecase domain.ApproveUsecaseInterface,
	userUsecase domain.UserUsecaseInterface,
	environment domain.EnvironmentLibraryInterface,
) domain.DocumentUsecaseInterface {
	return &documentUsecase{
		DocumentRepository: documentRepository,
		SettingUsecase:     settingUsecase,
		ApproveUsecase:     approveUsecase,
		Environment:        environment,
		UserUsecase:        userUsecase,
	}
}

func (u *documentUsecase) GetDocument(filter model.DocumentFilter) ([]model.DocumentApprove, error) {
	documents, err := u.DocumentRepository.GetDocument(filter)
	if err != nil {
		return []model.DocumentApprove{}, err
	}
	return documents, nil
}

func (u *documentUsecase) Create(id int) error {
	transection, err := u.DocumentRepository.GetTransectionProcess(id)
	if err != nil {
		return err
	}

	if transection.ID == 0 {
		return nil
	}

	documents, err := u.GetDocument(model.DocumentFilter{
		RefID: transection.RefID,
	})
	if err != nil {
		return err
	}

	if len(documents) > 0 {
		return nil
	}

	err = u.CreateDocument(transection)
	if err != nil {
		transection.Message = err.Error()
	} else {
		transection.Message = "Success."
	}

	err = u.DocumentRepository.UpdateTransection(transection)
	if err != nil {
		return err
	}

	return nil
}

func (u *documentUsecase) CreateDocument(transection model.ProcessCommand) error {
	document := model.DocumentApprove{}
	switch strings.TrimSpace(transection.Payload) {
	case document_type_enums.PO.String():
		pr, err := u.DocumentRepository.GetPRFormulaDocumentData(transection.RefID)
		if err != nil {
			return err
		}

		if pr.Status != "1" {
			return errors.New("STATUS_NOT_1")
		}

		document.DocumentRefID = pr.ID
		document.DocumentCode = pr.Code
		document.DocumentRefNo = pr.RefNo
		document.TypeCode = strings.TrimSpace(transection.Payload)
		document.BookID = pr.BookID
		document.BookCode = pr.BookCode
		document.CorpID = pr.CorpID
		document.BranchID = pr.BranchID
		document.DocumentDate = pr.DocumentDateTime
	case document_type_enums.PR.String():
		pr, err := u.DocumentRepository.GetPRFormulaDocumentData(transection.RefID)
		if err != nil {
			return err
		}

		if pr.Status != "1" {
			return errors.New("STATUS_NOT_1")
		}

		document.DocumentRefID = pr.ID
		document.DocumentCode = pr.Code
		document.DocumentRefNo = pr.RefNo
		document.TypeCode = strings.TrimSpace(transection.Payload)
		document.BookID = pr.BookID
		document.BookCode = pr.BookCode
		document.CorpID = pr.CorpID
		document.BranchID = pr.BranchID
		document.DocumentDate = pr.DocumentDateTime
	case document_type_enums.PB.String():
		pr, err := u.DocumentRepository.GetPBFormulaDocumentData(transection.RefID)
		if err != nil {
			return err
		}

		document.DocumentRefID = pr.ID
		document.DocumentCode = pr.Code
		document.DocumentRefNo = pr.RefNo
		document.TypeCode = strings.TrimSpace(transection.Payload)
		document.BookID = strings.TrimSpace(pr.BookID)
		document.BookCode = strings.TrimSpace(pr.BookCode)
		document.CorpID = strings.TrimSpace(pr.CorpID)
		document.BranchID = strings.TrimSpace(pr.BranchID)
		document.DocumentDate = pr.DocumentDateTime
	default:
		return nil
	}

	settings, err := u.SettingUsecase.GetSetting(model.SettingFilter{
		TypeCode: document.TypeCode,
		BookID:   document.BookID,
		CorpID:   document.CorpID,
		BranchID: document.BranchID,
	})

	if err != nil {
		return err
	}
	if len(settings) <= 0 {
		settings, err = u.SettingUsecase.GetSetting(model.SettingFilter{
			TypeCode: document.TypeCode,
			BookID:   "ALL",
			CorpID:   document.CorpID,
			BranchID: document.BranchID,
		})

		if err != nil {
			return err
		}
	}
	if len(settings) <= 0 {
		return errors.New("SETTING_NOT_FOUND")
	}
	setting := settings[0]

	approveList := []model.DocumentApproveList{}
	if setting.ApprovalPositionID01 > 0 {
		approves, err := u.ApproveUsecase.GetApproval(model.ApprovalFilter{
			PositionID: setting.ApprovalPositionID01,
		})
		if err != nil {
			return err
		}

		if len(approves) > 0 {
			document.PositionID01 = setting.ApprovalPositionID01
			document.PositionName01 = setting.ApprovalPositionName01
		}

		for _, v := range approves {
			id := uuid.New()
			approveList = append(approveList, model.DocumentApproveList{
				ApprovalID:        v.ID,
				ApprovalFirstName: v.FirstName,
				ApprovalLastName:  v.LastName,
				ApprovalEmail:     v.Email,
				ApprovalLevel:     1,
				PositionID:        document.PositionID01,
				PositionName:      document.PositionName01,
				TransactionKey:    id.String(),
			})
		}
	}

	if setting.ApprovalPositionID02 > 0 {
		approves, err := u.ApproveUsecase.GetApproval(model.ApprovalFilter{
			PositionID: setting.ApprovalPositionID02,
		})
		if err != nil {
			return err
		}

		if len(approves) > 0 {
			document.PositionID02 = setting.ApprovalPositionID02
			document.PositionName02 = setting.ApprovalPositionName02
		}

		for _, v := range approves {
			id := uuid.New()
			approveList = append(approveList, model.DocumentApproveList{
				ApprovalID:        v.ID,
				ApprovalFirstName: v.FirstName,
				ApprovalLastName:  v.LastName,
				ApprovalEmail:     v.Email,
				ApprovalLevel:     2,
				PositionID:        document.PositionID02,
				PositionName:      document.PositionName02,
				TransactionKey:    id.String(),
			})
		}
	}

	if setting.ApprovalPositionID03 > 0 {
		approves, err := u.ApproveUsecase.GetApproval(model.ApprovalFilter{
			PositionID: setting.ApprovalPositionID03,
		})
		if err != nil {
			return err
		}

		if len(approves) > 0 {
			document.PositionID03 = setting.ApprovalPositionID03
			document.PositionName03 = setting.ApprovalPositionName03
		}

		for _, v := range approves {
			id := uuid.New()
			approveList = append(approveList, model.DocumentApproveList{
				ApprovalID:        v.ID,
				ApprovalFirstName: v.FirstName,
				ApprovalLastName:  v.LastName,
				ApprovalEmail:     v.Email,
				ApprovalLevel:     3,
				PositionID:        document.PositionID03,
				PositionName:      document.PositionName03,
				TransactionKey:    id.String(),
			})
		}
	}

	if setting.ApprovalPositionID04 > 0 {
		approves, err := u.ApproveUsecase.GetApproval(model.ApprovalFilter{
			PositionID: setting.ApprovalPositionID04,
		})
		if err != nil {
			return err
		}

		if len(approves) > 0 {
			document.PositionID04 = setting.ApprovalPositionID04
			document.PositionName04 = setting.ApprovalPositionName04
		}

		for _, v := range approves {
			id := uuid.New()
			approveList = append(approveList, model.DocumentApproveList{
				ApprovalID:        v.ID,
				ApprovalFirstName: v.FirstName,
				ApprovalLastName:  v.LastName,
				ApprovalEmail:     v.Email,
				ApprovalLevel:     4,
				PositionID:        document.PositionID04,
				PositionName:      document.PositionName04,
				TransactionKey:    id.String(),
			})
		}
	}

	if setting.ApprovalPositionID05 > 0 {
		approves, err := u.ApproveUsecase.GetApproval(model.ApprovalFilter{
			PositionID: setting.ApprovalPositionID05,
		})
		if err != nil {
			return err
		}

		if len(approves) > 0 {
			document.PositionID05 = setting.ApprovalPositionID05
			document.PositionName05 = setting.ApprovalPositionName05
		}

		for _, v := range approves {
			id := uuid.New()
			approveList = append(approveList, model.DocumentApproveList{
				ApprovalID:        v.ID,
				ApprovalFirstName: v.FirstName,
				ApprovalLastName:  v.LastName,
				ApprovalEmail:     v.Email,
				ApprovalLevel:     5,
				PositionID:        document.PositionID05,
				PositionName:      document.PositionName05,
				TransactionKey:    id.String(),
			})
		}
	}

	stepCondition := 0
	if document.PositionID01 > 0 {
		stepCondition = 1
	} else if document.PositionID02 > 0 {
		stepCondition = 2
	} else if document.PositionID03 > 0 {
		stepCondition = 3
	} else if document.PositionID04 > 0 {
		stepCondition = 4
	} else if document.PositionID05 > 0 {
		stepCondition = 5
	}

	document.StepCondition = stepCondition
	document.DocumentStatus = document_status_enums.PENDING.String()
	document.ApproveConditon = setting.ApproveConditon

	err = u.DocumentRepository.CreateDocument(document, approveList)
	if err != nil {
		return err
	}

	return nil
}

func (u *documentUsecase) ApproveDocument(key string) error {
	approveData, err := u.DocumentRepository.GetApproveData(key)
	if err != nil {
		return err
	}
	documents, err := u.DocumentRepository.GetDocument(model.DocumentFilter{
		ID: int(approveData.DocumentID),
	})
	if err != nil {
		return err
	}
	if len(documents) <= 0 {
		return nil
	}
	document := documents[0]

	if document.DocumentStatus != document_status_enums.PENDING.String() {
		return error_enum.STATUS.Error()
	}

	if approveData.ApprovalLevel != document.StepCondition {
		return error_enum.STATUS.Error()
	}

	approveNumvers := 0
	if document.ApprovalStatus01 == document_status_enums.APPROVE.String() {
		approveNumvers++
	}
	if document.ApprovalStatus02 == document_status_enums.APPROVE.String() {
		approveNumvers++
	}
	if document.ApprovalStatus03 == document_status_enums.APPROVE.String() {
		approveNumvers++
	}
	if document.ApprovalStatus04 == document_status_enums.APPROVE.String() {
		approveNumvers++
	}
	if document.ApprovalStatus05 == document_status_enums.APPROVE.String() {
		approveNumvers++
	}

	approveNumvers++

	switch document.StepCondition {
	case 1:
		document.ApprovalID01 = approveData.ApprovalID
		document.ApprovalFirstName01 = approveData.ApprovalFirstName
		document.ApprovalLastName01 = approveData.ApprovalLastName
		document.ApprovalStatus01 = document_status_enums.APPROVE.String()
		document.ApprovalDate01 = u.Environment.Now()
	case 2:
		document.ApprovalID02 = approveData.ApprovalID
		document.ApprovalFirstName02 = approveData.ApprovalFirstName
		document.ApprovalLastName02 = approveData.ApprovalLastName
		document.ApprovalStatus02 = document_status_enums.APPROVE.String()
		document.ApprovalDate02 = u.Environment.Now()
	case 3:
		document.ApprovalID03 = approveData.ApprovalID
		document.ApprovalFirstName03 = approveData.ApprovalFirstName
		document.ApprovalLastName03 = approveData.ApprovalLastName
		document.ApprovalStatus03 = document_status_enums.APPROVE.String()
		document.ApprovalDate03 = u.Environment.Now()
	case 4:
		document.ApprovalID04 = approveData.ApprovalID
		document.ApprovalFirstName04 = approveData.ApprovalFirstName
		document.ApprovalLastName04 = approveData.ApprovalLastName
		document.ApprovalStatus04 = document_status_enums.APPROVE.String()
		document.ApprovalDate04 = u.Environment.Now()
	case 5:
		document.ApprovalID05 = approveData.ApprovalID
		document.ApprovalFirstName05 = approveData.ApprovalFirstName
		document.ApprovalLastName05 = approveData.ApprovalLastName
		document.ApprovalStatus05 = document_status_enums.APPROVE.String()
		document.ApprovalDate05 = u.Environment.Now()
	}

	if approveNumvers == document.ApproveConditon {
		document.DocumentStatus = document_status_enums.APPROVE.String()
	} else {

		stepCondition := 0
		for _, v := range []int{1, 2, 3, 4, 5} {
			if v <= document.StepCondition {
				continue
			}

			if stepCondition > 0 {
				continue
			}

			switch v {
			case 2:
				if document.PositionID02 > 0 {
					stepCondition = 2
				}
			case 3:
				if document.PositionID03 > 0 {
					stepCondition = 3
				}
			case 4:
				if document.PositionID04 > 0 {
					stepCondition = 4
				}
			case 5:
				if document.PositionID05 > 0 {
					stepCondition = 5
				}
			}
		}
		document.StepCondition = stepCondition
	}

	err = u.DocumentRepository.UpdateDocument(document)
	if err != nil {
		return err
	}
	return nil
}

func (u *documentUsecase) RejectDocument(key string) error {

	approveData, err := u.DocumentRepository.GetApproveData(key)
	if err != nil {
		return err
	}
	documents, err := u.DocumentRepository.GetDocument(model.DocumentFilter{
		ID: int(approveData.DocumentID),
	})
	if err != nil {
		return err
	}
	if len(documents) <= 0 {
		return nil
	}
	document := documents[0]

	if document.DocumentStatus != document_status_enums.PENDING.String() {
		return error_enum.STATUS.Error()
	}

	if approveData.ApprovalLevel != document.StepCondition {
		return error_enum.STATUS.Error()
	}

	switch document.StepCondition {
	case 1:
		document.ApprovalID01 = approveData.ApprovalID
		document.ApprovalFirstName01 = approveData.ApprovalFirstName
		document.ApprovalLastName01 = approveData.ApprovalLastName
		document.ApprovalStatus01 = document_status_enums.REJECT.String()
		document.ApprovalDate01 = u.Environment.Now()
	case 2:
		document.ApprovalID02 = approveData.ApprovalID
		document.ApprovalFirstName02 = approveData.ApprovalFirstName
		document.ApprovalLastName02 = approveData.ApprovalLastName
		document.ApprovalStatus02 = document_status_enums.REJECT.String()
		document.ApprovalDate02 = u.Environment.Now()
	case 3:
		document.ApprovalID03 = approveData.ApprovalID
		document.ApprovalFirstName03 = approveData.ApprovalFirstName
		document.ApprovalLastName03 = approveData.ApprovalLastName
		document.ApprovalStatus03 = document_status_enums.REJECT.String()
		document.ApprovalDate03 = u.Environment.Now()
	case 4:
		document.ApprovalID04 = approveData.ApprovalID
		document.ApprovalFirstName04 = approveData.ApprovalFirstName
		document.ApprovalLastName04 = approveData.ApprovalLastName
		document.ApprovalStatus04 = document_status_enums.REJECT.String()
		document.ApprovalDate04 = u.Environment.Now()
	case 5:
		document.ApprovalID05 = approveData.ApprovalID
		document.ApprovalFirstName05 = approveData.ApprovalFirstName
		document.ApprovalLastName05 = approveData.ApprovalLastName
		document.ApprovalStatus05 = document_status_enums.REJECT.String()
		document.ApprovalDate05 = u.Environment.Now()
	}

	document.DocumentStatus = document_status_enums.REJECT.String()
	err = u.DocumentRepository.UpdateDocument(document)
	if err != nil {
		return err
	}

	return nil
}

func (u *documentUsecase) GetInformation(filter model.FilterInformation) ([]model.DocumentInformation, error) {
	return u.DocumentRepository.GetInformation(filter)
}

func (u *documentUsecase) InformationDocument(req model.RequestDocumentInformation) error {

	approveData, err := u.DocumentRepository.GetApproveData(req.Key)
	if err != nil {
		return err
	}
	documents, err := u.DocumentRepository.GetDocument(model.DocumentFilter{
		ID: int(approveData.DocumentID),
	})
	if err != nil {
		return err
	}
	if len(documents) <= 0 {
		return nil
	}

	document := documents[0]
	if document.DocumentStatus != document_status_enums.PENDING.String() {
		return error_enum.STATUS.Error()
	}

	if approveData.ApprovalLevel != document.StepCondition {
		return error_enum.STATUS.Error()
	}

	information := model.DocumentInformation{
		DocumentID:        document.ID,
		ApprovalID:        approveData.ApprovalID,
		ApprovalFirstName: approveData.ApprovalFirstName,
		ApprovalLastName:  approveData.ApprovalLastName,
		InformationType:   "Q",
		Message:           req.Message,
	}

	document.DocumentStatus = document_status_enums.INFOMATION.String()
	err = u.DocumentRepository.UpdateDocumentInformation(document, information)
	if err != nil {
		return err
	}

	return nil
}

func (u *documentUsecase) ReptyInformationDocument(req model.ReptyDocumentInformation) error {

	user, err := u.UserUsecase.GetUserByID(uint(req.UserID))
	if err != nil {
		return err
	}

	documents, err := u.DocumentRepository.GetDocument(model.DocumentFilter{
		ID: req.ID,
	})
	if err != nil {
		return err
	}
	if len(documents) <= 0 {
		return nil
	}

	document := documents[0]
	if document.DocumentStatus != document_status_enums.INFOMATION.String() {
		return error_enum.STATUS.Error()
	}

	information := model.DocumentInformation{
		DocumentID:        document.ID,
		ApprovalID:        user.ID,
		ApprovalFirstName: user.FirstName,
		ApprovalLastName:  user.LastName,
		InformationType:   "A",
		Message:           req.Message,
	}

	document.DocumentStatus = document_status_enums.PENDING.String()
	err = u.DocumentRepository.UpdateDocumentInformation(document, information)
	if err != nil {
		return err
	}

	return nil
}

func (u *documentUsecase) ResetDocument(id int) error {

	transection, err := u.DocumentRepository.GetTransectionProcess(id)
	if err != nil {
		return err
	}

	if transection.ID == 0 {
		return nil
	}

	documents, err := u.DocumentRepository.GetDocument(model.DocumentFilter{
		RefID: transection.RefID,
	})
	if err != nil {
		return err
	}
	if len(documents) > 0 {
		document := documents[0]
		if document.DocumentStatus == document_status_enums.APPROVE.String() {
			return error_enum.STATUS.Error()
		}

		if document.DocumentStatus == document_status_enums.REJECT.String() {
			return error_enum.STATUS.Error()
		}
	}

	err = u.DocumentRepository.DeleteDocumentByRefID(transection.RefID)
	if err != nil {
		return err
	}

	err = u.Create(id)
	if err != nil {
		return err
	}

	return nil
}
