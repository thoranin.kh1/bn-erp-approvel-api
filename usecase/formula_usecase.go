package usecase

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/model"
)

type formulaUsecase struct {
	FormulaRepository domain.FormulaRepositoryInterface
}

func NewFormulaUsecase(formulaRepository domain.FormulaRepositoryInterface) domain.FormulaUsecaseInterface {
	return &formulaUsecase{
		FormulaRepository: formulaRepository,
	}
}

func (u *formulaUsecase) GetCompany(filter model.CompanyFilterModel) ([]model.CompanyModel, error) {
	return u.FormulaRepository.GetCompany(filter)
}

func (u *formulaUsecase) GetBranch(filter model.BranchFilterModel) ([]model.BranchModel, error) {
	return u.FormulaRepository.GetBranch(filter)
}

func (u *formulaUsecase) Getbook(filter model.BookFilterModel) ([]model.BookModel, error) {
	return u.FormulaRepository.Getbook(filter)
}

func (u *formulaUsecase) GetbookByID(id string) (model.BookModel, error) {
	books, err := u.Getbook(model.BookFilterModel{ID: id})
	if err != nil {
		return model.BookModel{}, err
	}

	if len(books) > 0 {
		return books[0], nil
	}

	return model.BookModel{}, nil
}
