package usecase

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
)

type positionUsecase struct {
	PositionRepository domain.PositionRepositoryInterface
}

func NewpositonUsecase(
	positionRepository domain.PositionRepositoryInterface,
) domain.PositionUsecaseInterface {
	return &positionUsecase{
		PositionRepository: positionRepository,
	}
}

func (u *positionUsecase) GetPositon() ([]model.Position, error) {
	positions, err := u.PositionRepository.Get(model.PositionFilter{})
	if err != nil {
		return []model.Position{}, err
	}
	return positions, nil
}

func (u *positionUsecase) GetPositonByID(id int) (model.Position, error) {
	if id <= 0 {
		return model.Position{}, nil
	}
	positions, err := u.PositionRepository.Get(model.PositionFilter{
		ID: id,
	})
	if err != nil {
		return model.Position{}, err
	}
	if len(positions) <= 0 {
		return model.Position{}, nil
	}
	return positions[0], nil
}

func (u *positionUsecase) CreatePosition(req model.PositionAction) (model.Position, error) {
	data := model.Position{
		Name:   req.Name,
		Remark: req.Remark,
	}
	positon, err := u.PositionRepository.Create(data, req.Approvals)
	if err != nil {
		return model.Position{}, err
	}
	return positon, nil
}

func (u *positionUsecase) UpdatePosition(req model.PositionAction) (model.Position, error) {
	data, err := u.GetPositonByID(req.ID)
	if err != nil {
		return model.Position{}, err
	}

	data.Name = req.Name
	data.Remark = req.Remark

	positon, err := u.PositionRepository.Update(data, req.Approvals)
	if err != nil {
		return model.Position{}, err
	}
	return positon, nil
}

func (u *positionUsecase) DeletePosition(id int) error {
	data, err := u.GetPositonByID(id)
	if err != nil {
		return err
	}

	if data.ID <= 0 {
		return nil
	}

	if data.Used {
		return error_enum.USED.Error()
	}

	err = u.PositionRepository.Delete(id)
	if err != nil {
		return err
	}
	return nil
}
