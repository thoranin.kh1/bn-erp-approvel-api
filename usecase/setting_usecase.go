package usecase

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
)

type settingUsecase struct {
	SettingRepository domain.SettingRepositoryInterface
	PositionUsecase   domain.PositionUsecaseInterface
	FormulaUsecase    domain.FormulaUsecaseInterface
}

func NewSettingUsecase(
	settingRepository domain.SettingRepositoryInterface,
	positionUsecase domain.PositionUsecaseInterface,
	formulaUsecase domain.FormulaUsecaseInterface,
) domain.SettingUsecaseInterface {
	return &settingUsecase{
		SettingRepository: settingRepository,
		PositionUsecase:   positionUsecase,
		FormulaUsecase:    formulaUsecase,
	}
}

func (u *settingUsecase) GetDocumentType() ([]model.DocumentType, error) {
	return u.SettingRepository.GetDocumentType()
}

func (u *settingUsecase) GetSetting(filter model.SettingFilter) ([]model.Setting, error) {
	return u.SettingRepository.GetSetting(filter)
}

func (u *settingUsecase) GetSettingByID(id uint) (model.Setting, error) {
	settings, err := u.GetSetting(model.SettingFilter{ID: id})
	if err != nil {
		return model.Setting{}, err
	}

	if len(settings) > 0 {
		return settings[0], nil
	}

	return model.Setting{}, nil
}

func (u *settingUsecase) MapSettingPositionData(request model.SettingRequest, setting model.Setting) (model.Setting, error) {

	positions := []uint{}

	setting.ApprovalPositionID01 = 0
	setting.ApprovalPositionName01 = ""
	if request.PositionID01 > 0 {
		position, _ := u.PositionUsecase.GetPositonByID(int(request.PositionID01))
		if position.ID > 0 {
			positions = append(positions, request.PositionID01)
			setting.ApprovalPositionID01 = position.ID
			setting.ApprovalPositionName01 = position.Name
		}
	}

	setting.ApprovalPositionID02 = 0
	setting.ApprovalPositionName02 = ""
	if request.PositionID02 > 0 {
		position, _ := u.PositionUsecase.GetPositonByID(int(request.PositionID02))
		if position.ID > 0 {
			positions = append(positions, request.PositionID02)
			setting.ApprovalPositionID02 = position.ID
			setting.ApprovalPositionName02 = position.Name
		}
	}

	setting.ApprovalPositionID03 = 0
	setting.ApprovalPositionName03 = ""
	if request.PositionID03 > 0 {
		position, _ := u.PositionUsecase.GetPositonByID(int(request.PositionID03))
		if position.ID > 0 {
			positions = append(positions, request.PositionID03)
			setting.ApprovalPositionID03 = position.ID
			setting.ApprovalPositionName03 = position.Name
		}
	}

	setting.ApprovalPositionID04 = 0
	setting.ApprovalPositionName04 = ""
	if request.PositionID04 > 0 {
		position, _ := u.PositionUsecase.GetPositonByID(int(request.PositionID04))
		if position.ID > 0 {
			positions = append(positions, request.PositionID04)
			setting.ApprovalPositionID04 = position.ID
			setting.ApprovalPositionName04 = position.Name
		}
	}

	setting.ApprovalPositionID05 = 0
	setting.ApprovalPositionName05 = ""
	if request.PositionID05 > 0 {
		position, _ := u.PositionUsecase.GetPositonByID(int(request.PositionID05))
		if position.ID > 0 {
			positions = append(positions, request.PositionID05)
			setting.ApprovalPositionID05 = position.ID
			setting.ApprovalPositionName05 = position.Name
		}
	}

	conditon := len(positions)
	setting.ApproveConditon = conditon

	return setting, nil
}

func (u *settingUsecase) CreateSetting(request model.SettingRequest) (model.Setting, error) {

	settings, err := u.GetSetting(model.SettingFilter{
		TypeCode: request.Type,
		BookID:   request.BookID,
		CorpID:   request.CorpID,
		BranchID: request.BranchID,
	})
	if err != nil {
		return model.Setting{}, err
	}

	if len(settings) > 0 {
		return model.Setting{}, error_enum.EXISTS.Error()
	}

	setting := model.Setting{}
	setting.Name = request.Name
	setting.CorpID = request.CorpID
	setting.BranchID = request.BranchID
	setting.TypeCode = request.Type
	setting.BookID = request.BookID

	if request.BookID != "ALL" {
		book, err := u.FormulaUsecase.GetbookByID(request.BookID)
		if err != nil {
			return model.Setting{}, err
		}

		setting.BookCode = book.Code
		setting.BookName = book.Name
	}

	setting, _ = u.MapSettingPositionData(request, setting)
	setting, err = u.SettingRepository.CreateSetting(setting)
	if err != nil {
		return model.Setting{}, err
	}
	return setting, nil
}

func (u *settingUsecase) UpdateSetting(request model.SettingRequest) (model.Setting, error) {
	setting, err := u.GetSettingByID(request.ID)
	if err != nil {
		return model.Setting{}, err
	}

	setting, _ = u.MapSettingPositionData(request, setting)
	setting, err = u.SettingRepository.UpdateSetting(setting)
	if err != nil {
		return model.Setting{}, err
	}

	return setting, nil
}

func (u *settingUsecase) DeleteSetting(id uint) error {
	err := u.SettingRepository.DeleteSetting(id)
	if err != nil {
		return err
	}

	return nil
}
