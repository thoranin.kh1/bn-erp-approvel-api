package usecase

import (
	"bn-erp-approvel-api/domain"
	"bn-erp-approvel-api/enum/error_enum"
	"bn-erp-approvel-api/model"
	"bn-erp-approvel-api/util"
	"fmt"
)

type userUsecase struct {
	UserRepository domain.UserRepositoryInterface
	Token          domain.TokenLibraryInterface
	RootPassword   string
}

func NewUserUsecase(
	userRepository domain.UserRepositoryInterface,
	token domain.TokenLibraryInterface,
	rootPassword string,
) domain.UserUsecaseInterface {
	return &userUsecase{
		UserRepository: userRepository,
		Token:          token,
		RootPassword:   rootPassword,
	}
}

func (u *userUsecase) GetUser() ([]model.User, error) {
	users, err := u.UserRepository.GetUser(model.UserFilter{})
	if err != nil {
		return []model.User{}, err
	}
	return users, nil
}

func (u *userUsecase) GetUserByID(id uint) (model.User, error) {
	if id == 999999 {
		return u.GetUserRoot(), nil
	}
	users, err := u.UserRepository.GetUser(model.UserFilter{
		ID: id,
	})
	if err != nil {
		return model.User{}, err
	}
	if len(users) <= 0 {
		return model.User{}, err
	}

	return users[0], nil
}

func (u *userUsecase) CreateUser(req model.UserCreate) (model.User, error) {

	password, err := util.HashPassword(req.Password)
	if err != nil {
		return model.User{}, err
	}

	userModel := model.User{
		Username:  req.Username,
		Password:  password,
		FirstName: req.FirstName,
		LastName:  req.LastName,
	}

	permissions := []model.Permission{}
	for _, v := range req.Permissions {
		permissions = append(permissions, model.Permission{
			Code: v,
		})
	}

	user, err := u.UserRepository.CreateUser(userModel, permissions)
	if err != nil {
		return model.User{}, err
	}

	return user, nil
}

func (u *userUsecase) UpdateUser(req model.UserUpdate) (model.User, error) {
	if req.ID <= 0 {
		return model.User{}, error_enum.EXPTY.Error()
	}
	user, err := u.GetUserByID(req.ID)
	if err != nil {
		return model.User{}, err
	}

	user.FirstName = req.FirstName
	user.LastName = req.LastName

	permissions := []model.Permission{}
	for _, v := range req.Permissions {
		permissions = append(permissions, model.Permission{
			UserID: user.ID,
			Code:   v,
		})
	}

	userData, err := u.UserRepository.UpdateUser(user, permissions)
	if err != nil {
		return model.User{}, err
	}

	return userData, nil
}

func (u *userUsecase) UpdateUserPasword(req model.UserPasswordUpdate) error {
	if req.ID <= 0 {
		return error_enum.EXPTY.Error()
	}
	user, err := u.GetUserByID(req.ID)
	if err != nil {
		return err
	}

	password, err := util.HashPassword(req.Password)
	if err != nil {
		return err
	}

	user.Password = password

	err = u.UserRepository.UpdateUserProfile(user)
	if err != nil {
		return err
	}

	return nil
}

func (u *userUsecase) DeleteUser(id uint) error {
	if id <= 0 {
		return error_enum.EXPTY.Error()
	}
	err := u.UserRepository.DeleteUser(id)
	if err != nil {
		return err
	}
	return nil
}

func (u *userUsecase) GetUserRoot() model.User {
	return model.User{
		ID:        999999,
		Username:  "def-root",
		FirstName: "default",
		LastName:  "User",
	}
}

func (u *userUsecase) SignIn(req model.UserSignin) (model.Token, error) {
	var user model.User

	if req.Username == "def-root" {
		passwordHash, err := util.HashPassword(u.RootPassword)
		if err != nil {
			return model.Token{}, fmt.Errorf("%w : %s", error_enum.PASSWORD.Error(), err)
		}

		if check := util.ValidatePasswordByHash(req.Password, passwordHash); !check {
			return model.Token{}, fmt.Errorf("%w", error_enum.PASSWORD.Error())
		}

		user = u.GetUserRoot()
	} else {
		users, err := u.UserRepository.GetUser(model.UserFilter{
			Username: req.Username,
		})
		if err != nil {
			return model.Token{}, fmt.Errorf("%w : %s", error_enum.MSSQL.Error(), err)
		}

		if len(users) <= 0 {
			return model.Token{}, fmt.Errorf("%w", error_enum.EXPTY.Error())
		}
		user = users[0]
		if check := util.ValidatePasswordByHash(req.Password, user.Password); !check {
			return model.Token{}, fmt.Errorf("%w", error_enum.PASSWORD.Error())
		}
	}
	tokenString, err := u.Token.CreateToken(model.TokenClaimsModel{
		ID:        user.ID,
		UserName:  user.Username,
		FirstName: user.FirstName,
		LastName:  user.LastName,
	})
	if err != nil {
		return model.Token{}, err
	}

	token, err := u.UserRepository.CreateToken(model.Token{
		UserID: user.ID,
		Token:  tokenString,
	})

	if err != nil {
		return model.Token{}, err
	}

	return token, nil
}

func (u *userUsecase) HasToken(key string) (bool, error) {
	token, err := u.UserRepository.GetToken(key)
	if err != nil {
		return false, err
	}
	return token.ID > 0, nil
}

func (u *userUsecase) DeleteToken(key string) error {
	err := u.UserRepository.DeleteToken(key)
	if err != nil {
		return err
	}
	return nil
}

func (u *userUsecase) GetMenu() ([]model.Menu, error) {
	return u.UserRepository.GetMenu()
}

func (u *userUsecase) InitMenu() error {
	menus := []string{
		"DOCUMENT_APPROVE_MONITOR",
		"APPROVAL",
		"APPROVAL_SETTING",
		"POSITION",
		"USER_MANAGE",
	}
	err := u.UserRepository.CreateOrUpdateMenu(menus)
	if err != nil {
		return err
	}
	return nil
}

func (u *userUsecase) GetUserPermission(userID int) ([]model.Permission, error) {
	if userID == 999999 {
		return []model.Permission{
			{
				Code: "USER_MANAGE",
			},
		}, nil
	}
	return u.UserRepository.GetUserPermission(userID)
}
