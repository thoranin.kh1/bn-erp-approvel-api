package util

import (
	"crypto/aes"
	"crypto/cipher"
	"crypto/rand"
	"encoding/base64"
	"errors"
	"strings"

	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, error) {
	passwordEncypt, err := bcrypt.GenerateFromPassword([]byte(password), 14)
	return string(passwordEncypt), err
}

func ValidatePasswordByHash(password string, hash string) bool {
	err := bcrypt.CompareHashAndPassword([]byte(hash), []byte(password))
	return err == nil
}

func QueryEncode(s string) string {
	b64 := base64.URLEncoding.EncodeToString([]byte(s))
	b64 = strings.ReplaceAll(b64, "+", ".")
	b64 = strings.ReplaceAll(b64, "/", "_")
	b64 = strings.ReplaceAll(b64, "=", "-")
	return b64
}

func QueryDecode(b64 string) string {
	b64 = strings.ReplaceAll(b64, ".", "+")
	b64 = strings.ReplaceAll(b64, "_", "/")
	b64 = strings.ReplaceAll(b64, "-", "=")
	rawDecodedText, err := base64.URLEncoding.DecodeString(b64)
	if err != nil {
		return "D_ERR" + err.Error()
	}
	return string(rawDecodedText)
}

func B64Encode(s string) string {
	b64 := base64.StdEncoding.EncodeToString([]byte(s))
	b64 = strings.ReplaceAll(b64, "+", ".")
	b64 = strings.ReplaceAll(b64, "/", "_")
	b64 = strings.ReplaceAll(b64, "=", "-")
	return b64
}

func B64Decode(b64 string) string {
	b64 = strings.ReplaceAll(b64, ".", "+")
	b64 = strings.ReplaceAll(b64, "_", "/")
	b64 = strings.ReplaceAll(b64, "-", "=")
	rawDecodedText, err := base64.StdEncoding.DecodeString(b64)
	if err != nil {
		return "D_ERR" + err.Error()
	}
	return string(rawDecodedText)
}

func Encrypt(id string, pt string) (string, error) {
	if id == "" {
		return "", errors.New("ID_IS_EMPTY")
	}
	key := id + "96389fc5d84b8935de29ea5be831b8d924ed730813da45548404c733b01b8af2"
	key = key[:32]

	c, err := encryptAES([]byte(key), pt)
	if err != nil {
		return "", err
	}
	return c, nil
}

func Decrypt(id string, pt string) (string, error) {
	if id == "" {
		return "", errors.New("ID_IS_EMPTY")
	}
	key := id + "96389fc5d84b8935de29ea5be831b8d924ed730813da45548404c733b01b8af2"
	key = key[:32]

	c, err := decryptAES([]byte(key), pt)
	if err != nil {
		return "", err
	}
	return c, nil
}

func EncryptServer(pt string) (string, error) {
	key := "a7949d93a389fe7af8e719eaea3a8b277d5e4113f66c7105cf2b7edb17c7589c"
	key = key[:32]

	c, err := encryptAES([]byte(key), pt)
	if err != nil {
		return "", err
	}
	return c, nil
}

func DecryptServer(pt string) (string, error) {
	key := "a7949d93a389fe7af8e719eaea3a8b277d5e4113f66c7105cf2b7edb17c7589c"
	key = key[:32]

	c, err := decryptAES([]byte(key), pt)
	if err != nil {
		return "", err
	}
	return c, nil
}

func encryptAES(key []byte, plaintext string) (string, error) {
	aes, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(aes)
	if err != nil {
		return "", err
	}

	nonce := make([]byte, gcm.NonceSize())
	_, err = rand.Read(nonce)
	if err != nil {
		return "", err
	}

	ciphertext := gcm.Seal(nonce, nonce, []byte(plaintext), nil)
	return string(ciphertext), nil
}

func decryptAES(key []byte, ciphertext string) (string, error) {
	aes, err := aes.NewCipher(key)
	if err != nil {
		return "", err
	}

	gcm, err := cipher.NewGCM(aes)
	if err != nil {
		return "", err
	}

	nonceSize := gcm.NonceSize()
	nonce, ciphertext := ciphertext[:nonceSize], ciphertext[nonceSize:]

	plaintext, err := gcm.Open(nil, []byte(nonce), []byte(ciphertext), nil)
	if err != nil {
		return "", err
	}

	return string(plaintext), nil
}
