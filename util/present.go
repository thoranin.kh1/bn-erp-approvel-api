package util

import (
	"fmt"
	"strconv"
	"strings"
)

func DateStringToSQL(date string) string {
	dateSplit := strings.Split(date, "-")
	if len(dateSplit) < 3 {
		return ""
	}
	return fmt.Sprintf("%s-%s-%s", dateSplit[2], dateSplit[1], dateSplit[0])
}

func DateStringToThai(date string) string {
	format := ""
	if date == "" {
		return ""
	}
	dateSplit := strings.Split(date, "-")
	if len(dateSplit) < 3 {
		return ""
	}
	intVar, err := strconv.Atoi(dateSplit[0])
	if err != nil {
		return ""
	}
	format = fmt.Sprintf("%s/%s/%d", dateSplit[2], dateSplit[1], (intVar + 543))
	return format
}
